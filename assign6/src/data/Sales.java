package data;

import java.sql.Date;

/**
 * Created by IntelliJ IDEA.
 * User: weskinne
 * Date: 3/9/11
 * Time: 3:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sales {
    public int sonum;
    public String stor_id;
    public String ponum;
    public Date sdate;

    public Sales(int sonum, String stor_id, String ponum, Date sdate) {
        this.sonum = sonum;
        this.stor_id = stor_id;
        this.ponum = ponum;
        this.sdate = sdate;
    }

    public Sales() {
    }
}
