package data;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Repository<T> {
    private Connection conn;
    private Class genericType;
    private String typeName;

    public Repository() throws SQLException, ClassNotFoundException {
        genericType = ((Class) ((ParameterizedType) ((Object) this).getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        typeName = genericType.getSimpleName();

        /* Load the database driver. */
            Class.forName("org.postgresql.Driver");

        conn =  DriverManager.getConnection("jdbc:postgresql://localhost/weskinne", "weskinne", "wes7894");
    }

    public void Insert(T record) throws Exception {
        Field[] fields = record.getClass().getDeclaredFields();

        Statement st = conn.createStatement();
        String sql = String.format("INSERT INTO %s ", typeName);

        String columns = "(";
        String values = ") VALUES (";
        for (int i = 0; i < fields.length; i++) {
            String column = fields[i].getName();
            String value;
            try {
                value = fields[i].get(record).toString();
            }
            catch (Exception e) {
                value = null;
            }
            if (value != null) {
                columns += column + ",";
                values += String.format("'%s',", value);
            }
        }
        columns = columns.substring(0, columns.length() - 1);
        values = values.substring(0, values.length() - 1);
        values += ")";
        sql = sql + columns + values;

        st.execute(sql);
        st.close();
    }

    public void Update(T record, String pkeyName, String pkeyValue) throws Exception {
        Field[] fields = record.getClass().getDeclaredFields();

        Statement st = conn.createStatement();
        String sql = String.format("UPDATE %s SET ", typeName);

        for (int i = 0; i < fields.length; i++) {
            String column = fields[i].getName();
            String value;
            try {
                value = fields[i].get(record).toString();
            }
            catch (Exception e) {
                value = null;
            }
            if (value != null) {
                if (i == fields.length - 1)
                    sql += String.format("%s='%s'", column, value);
                else
                    sql += String.format("%s='%s',", column, value);
            }
        }

        sql += String.format("WHERE %s=%s",pkeyName,pkeyValue);

        st.execute(sql);
        st.close();
    }

    public void InsertOrUpdate(T record, String pkeyName, String pkeyValue) throws Exception {
        if (Find(pkeyName,pkeyValue) == null) {
            Insert(record);
        } else {
            Update(record,pkeyName,pkeyValue);
        }
    }

    public T Find(String pkeyName, String pkeyValue) throws Exception {
        Object record = null;

        record = genericType.newInstance();


        Field[] fields = record.getClass().getFields();

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(String.format("SELECT * FROM %s WHERE %s='%s'", typeName, pkeyName, pkeyValue));
        if (rs.next()) {

            for (Field field : fields) {
                String fieldName = field.getName();

                field.set(record, rs.getObject(fieldName));

            }
        } else {
            return null;
        }

        rs.close();
        st.close();

        return (T) record;
    }

    public List<T> FindAll() throws Exception {
        ArrayList<T> records = new ArrayList<T>();

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(String.format("SELECT * FROM %s", typeName));
        while (rs.next()) {
            Object record = null;
            try {
                record = genericType.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Field[] fields = record.getClass().getFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                field.set(record, rs.getObject(fieldName));
            }
            records.add((T) record);
        }

        rs.close();
        st.close();

        if (records.isEmpty()) {
            return null;
        }
        return records;
    }

    public void Delete(int id) throws Exception {
        Statement st = conn.createStatement();
        st.execute(String.format("DELETE FROM %s WHERE Id=%s", typeName, id));
        st.close();
    }

    public T WhereSingle(String column, String comparator, String value) throws Exception {
        Object record;
        record = genericType.newInstance();
        Field[] fields = record.getClass().getFields();

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(String.format("SELECT * FROM %s WHERE %s%s'%s'", typeName, column, comparator, value));
        if (rs.next()) {

            for (Field field : fields) {
                String fieldName = field.getName();

                field.set(record, rs.getObject(fieldName));

            }
        } else {
            return null;
        }

        rs.close();
        st.close();

        return (T) record;
    }

    public List<T> Where(String column, String comparator, String value) throws Exception {
        ArrayList<T> records = new ArrayList<T>();

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(String.format("SELECT * FROM %s WHERE %s%s'%s'", typeName, column, comparator, value));
        while (rs.next()) {
            Object record = null;
            try {
                record = genericType.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Field[] fields = record.getClass().getFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                field.set(record, rs.getObject(fieldName));
            }
            records.add((T) record);
        }

        rs.close();
        st.close();

        return records;
    }
}

