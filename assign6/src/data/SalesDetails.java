package data;

import java.sql.Date;

/**
 * Created by IntelliJ IDEA.
 * User: weskinne
 * Date: 3/9/11
 * Time: 3:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class SalesDetails {

    public int sonum;
    public int qty_ordered;
    public int qty_shipped;
    public String title_id;
    public Date date_shipped;

    public SalesDetails(int sonum, int qty_ordered, int qty_shipped, String title_id, Date date_shipped) {
        this.sonum = sonum;
        this.qty_ordered = qty_ordered;
        this.qty_shipped = qty_shipped;
        this.title_id = title_id;
        this.date_shipped = date_shipped;
    }

    public SalesDetails() {
    }
}
