<%@ page import="data.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Assign6: Sales Order Update.</title>
</head>

<body>
<%
    String statusMsg = "";
    Sales sales = new Sales();
    SalesDetails salesDetails = new SalesDetails();
    try {
        SalesRepository salesRepository = new SalesRepository();
        SalesDetailsRepository salesDetailsRepository = new SalesDetailsRepository();

        String sonum = request.getParameter("sonum");
        int qty_ordered = Integer.parseInt(request.getParameter("qty_ordered"));

        sales = salesRepository.Find("sonum", sonum);
        salesDetails = salesDetailsRepository.Find("sonum", sonum);

        salesDetails.qty_ordered = qty_ordered;

        salesDetailsRepository.Update(salesDetails,"sonum",sonum);

        statusMsg += "Sale Details Updated!";
    }
    catch( Exception e ) {
        statusMsg += e.getMessage() + "\n\n\n";
    }
%>

<div style="text-align: center;">
    <h1>Assign6: Sales Order Update.</h1>

    <p><%= statusMsg%></p>

</div>

</body>
</html>