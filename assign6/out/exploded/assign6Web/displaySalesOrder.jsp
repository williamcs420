<%@ page import="data.Sales" %>
<%@ page import="data.SalesDetailsRepository" %>
<%@ page import="data.SalesRepository" %>
<%@ page import="data.SalesDetails" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Assign6: Display Sales Order.</title>
    <style type="">
        table {
            background-color: gold;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>

<body>
<%
    String exception = "";
    Sales sales = new Sales();
    SalesDetails salesDetails = new SalesDetails();
    try {
        SalesRepository salesRepository = new SalesRepository();
        SalesDetailsRepository salesDetailsRepository = new SalesDetailsRepository();

        String FRM_sonum = request.getParameter("FRM_sonum");

        sales = salesRepository.Find("sonum", FRM_sonum);
        salesDetails = salesDetailsRepository.Find("sonum", FRM_sonum);
    }
    catch( Exception e ) {
        exception += e.getMessage() + "\n\n\n";
        exception += e.getStackTrace();

    }


%>

<p><%= exception %></p>

<form action="salesOrderUpdate.jsp" method="post">

Sale
<table>
    <tr>
        <td>sonum</td>
        <td><%= sales.sonum %><input type="hidden" name="sonum" value="<%= sales.sonum %>"/>
        </td>
    </tr>
    <tr>
        <td>stor_id</td>
        <td><%= sales.stor_id %>
        </td>
    </tr>
    <tr>
        <td>ponum</td>
        <td><%= sales.ponum %>
        </td>
    </tr>
    <tr>
        <td>date</td>
        <td><%= sales.sdate %>
        </td>
    </tr>
</table>
  <br>
Sale Details
<table>
    <tr>
        <td>sonum</td>
        <td><%= salesDetails.sonum %>
        </td>
    </tr>
    <tr>
        <td>qty_ordered</td>
        <td><input type="text" value="<%= salesDetails.qty_ordered%>" name="qty_ordered"/>
        </td>
    </tr>
    <tr>
        <td>qty_shipped</td>
        <td><%= salesDetails.qty_shipped%>
        </td>
    </tr>
    <tr>
        <td>title_id</td>
        <td><%= salesDetails.title_id %>
        </td>
    </tr>
    <tr>
        <td>date_shipped</td>
        <td><%= salesDetails.date_shipped%>
        </td>
    </tr>
</table>
    <br>
    <span style="float: right"><input type="submit" /> </span>

    </form>

</body>
</html>