
public class User{

    protected Integer userid;
    protected String lastName;
    protected String firstName;
    protected String otherNames;

    public User() {
    }

    public User(Integer userid) {
        this.userid = userid;
    }

    public User(Integer userid, String lastName, String firstName, String otherNames) {
        this.userid = userid;
        this.lastName = lastName;
        this.firstName = firstName;
        this.otherNames = otherNames.length() == 0 ? "(none)" : otherNames;
    }

    /*---------------------------------------------------------------------------*/
    public Integer getUserid(){
        return userid;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    /*---------------------------------------------------------------------------*/
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }
}
