import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Date;

public class LoadStudents {

    public void clearDatabase(ObjectContainer db) {
      ObjectSet result=db.queryByExample(new Student());
      while(result.hasNext()) {
        db.delete(result.next());
      }
    }

    private void processInput(ObjectContainer db) throws Exception {
        boolean debug= false;

        /* Open the data file. */
        BufferedReader input = new BufferedReader(new FileReader("students.data"));

        /* Read and process each line which is an Student. */
        String line;
        while ((line = input.readLine()) != null) {
            /* Extract the information from the line. */
            String[] factor = line.split("[|]");
            int studentid = new Integer(factor[0].trim());
            String lastName = factor[1].trim();
            String firstName = factor[2].trim();
            String middleName = factor[3].trim();
            if(debug)System.out.println(String.format("%s %s %s %s",studentid,lastName,firstName,middleName));

            /* Add the information as an Student to the database. */
            db.store(new Student(studentid,lastName,firstName,middleName));
        }
    }

    public void retrieveAllStudents(ObjectContainer db) {
      Query query=db.query();
      query.constrain(Student.class);
      ObjectSet result=query.execute();
      while(result.hasNext()) {
          Student s= (Student)result.next();
          int studentId = s.getUserid();
          String firstName = s.getFirstName();
          String lastName = s.getLastName();
          String middleName = s.getMiddleName();

          System.out.println(String.format(" Student: %s | %s | %s | %s ",studentId,lastName,firstName,middleName));
      }
    }

    private void run() {
        ObjectContainer db = null;
        try {
            /* Open the database. */
            db = Db4o.openFile("ceis.db4o");

            /* Remove all Event objects from the database. */
            clearDatabase(db);

            /* Process the raw data. */
            processInput(db);

            /* Commit all additions. */
            db.commit();

            /* Verify that we have the objects. */
            retrieveAllStudents(db);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Cleanup. */
            if (db != null) db.close();
        }
    }

    public static void main(String[] args) {
        LoadStudents application = new LoadStudents();
        application.run();
    }
}
