/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 4/13/11
 * Time: 3:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class Student extends User {

    private String middleName;

    public Student() {

    }

    public Student(Integer userid, String lastName, String firstName, String middleName) {
        super(userid,lastName,firstName,"");
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
