import java.util.Date;

public class Event {
    private Short eventid;
    private String title;
    private String location;
    private Date date;
    private Boolean onCampus;
    private Short credit;

    public Event(){
    }

    public Event(Short eventid, String title, String location, Date date, Boolean onCampus, Short credit){
        this.eventid= eventid;
        this.title= title;
        this.location= location;
        this.date= date;
        this.onCampus= onCampus;
        this.credit= credit;
    }

    public Short getEventid() {
        return eventid;
    }

    public void setEventid(Short eventid) {
        this.eventid = eventid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getOnCampus() {
        return onCampus;
    }

    public void setOnCampus(Boolean onCampus) {
        this.onCampus = onCampus;
    }

    public Short getCredit() {
        return credit;
    }

    public void setCredit(Short credit) {
        this.credit = credit;
    }
}
