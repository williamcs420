import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;

import javax.sql.rowset.Predicate;

public class AttendanceReport {


    public void retrieveAttendanceReport(ObjectContainer db) {
        Query query = db.query();
        query.constrain(Student.class);
        /* sort the students by their last name */
        query.descend("lastName").orderAscending();
        ObjectSet result = query.execute();
        while (result.hasNext()) {
            Student s = (Student) result.next();
            int studentId = s.getUserid();
            String firstName = s.getFirstName();
            String lastName = s.getLastName();
            String middleName = s.getMiddleName();

            /* we have the student, now lets get their attendance report */

            /* get the attendance record */
            Query aq = db.query();
            aq.constrain(Attendance.class);
            aq.descend("studentid").constrain(studentId);
            ObjectSet aqResult = aq.execute();

            int attendanceCount = aqResult.size();

            System.out.println(String.format("Events Attended:  %s | %s | %s | %s | %s ", studentId, lastName, firstName, middleName, attendanceCount));
        }
    }

    private void run() {
        ObjectContainer db = null;
        try {
            /* Open the database. */
            db = Db4o.openFile("ceis.db4o");

            /* get the attendance report */
            retrieveAttendanceReport(db);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Cleanup. */
            if (db != null) db.close();
        }
    }

    public static void main(String[] args) {
        AttendanceReport application = new AttendanceReport();
        application.run();
    }
}
