public class Attendance {
    private int studentid;
    private short eventid;
    private short type;

    public Attendance() {

    }

    public Attendance(int studentid, short eventid, short type) {
        this.studentid = studentid;
        this.eventid = eventid;
        this.type = type;
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    public short getEventid() {
        return eventid;
    }

    public void setEventid(short eventid) {
        this.eventid = eventid;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }
}
