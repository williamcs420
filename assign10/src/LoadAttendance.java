import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;

import java.io.BufferedReader;
import java.io.FileReader;

public class LoadAttendance {

    public void clearDatabase(ObjectContainer db) {
      ObjectSet result=db.queryByExample(new Attendance());
      while(result.hasNext()) {
        db.delete(result.next());
      }
    }

    private void processInput(ObjectContainer db) throws Exception {
        boolean debug= false;

        /* Open the data file. */
        BufferedReader input = new BufferedReader(new FileReader("attendance.data"));

        /* Read and process each line which is an Attendance. */
        String line;
        while ((line = input.readLine()) != null) {
            /* Extract the information from the line. */
            String[] factor = line.split("[|]");
            int studentid = new Integer(factor[0].trim());
            short eventid = new Short(factor[1].trim());
            short type = new Short(factor[2].trim());
            if(debug)System.out.println(String.format("%s %s %s ",studentid,eventid,type));

            /* Add the information as an Attendance to the database. */
            db.store(new Attendance(studentid,eventid,type));
        }
    }

    public void retrieveAllAttendance(ObjectContainer db) {
      Query query=db.query();
      query.constrain(Attendance.class);
      ObjectSet result=query.execute();
      while(result.hasNext()) {
          Attendance attend = (Attendance)result.next();
          int studentId = attend.getStudentid();
          short eventId = attend.getEventid();
          short type = attend.getType();

          System.out.println(String.format(" Attendance: %s | %s | %s ",studentId,eventId,type));
      }
    }

    private void run() {
        ObjectContainer db = null;
        try {
            /* Open the database. */
            db = Db4o.openFile("ceis.db4o");

            /* Remove all Attendance objects from the database. */
            clearDatabase(db);

            /* Process the raw data. */
            processInput(db);

            /* Commit all additions. */
            db.commit();

            /* Verify that we have the objects. */
            retrieveAllAttendance(db);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Cleanup. */
            if (db != null) db.close();
        }
    }

    public static void main(String[] args) {
        LoadAttendance application = new LoadAttendance();
        application.run();
    }
}
