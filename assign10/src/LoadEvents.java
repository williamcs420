import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoadEvents {

    public void clearDatabase(ObjectContainer db) {
      ObjectSet result=db.queryByExample(new Event());
      while(result.hasNext()) {
        db.delete(result.next());
      }
    }

    private void processInput(ObjectContainer db) throws Exception {
        boolean debug= false;

        /* Open the data file. */
        BufferedReader input = new BufferedReader(new FileReader("events.data"));

        /* Read and process each line which is an Event. */
        String line;
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        while ((line = input.readLine()) != null) {
            /* Extract the information from the line. */
            String[] factor = line.split("[|]");
            Short eventid = new Short(factor[0].trim());
            String title = factor[1].trim();
            String location = factor[2].trim();
            Date date = dateFormatter.parse(factor[3].trim(), new ParsePosition(0));
            Boolean onCampus = factor[4].trim().equals("t");
            Short credit= new Short(factor[5].trim());
            if(debug)System.out.println(String.format("%d %s %s %s %b %d",eventid,title,location,date.toString(),onCampus,credit));

            /* Add the information as an Event to the database. */
            db.store(new Event(eventid,title,location,date,onCampus,credit));
        }
    }

    public void retrieveAllEvents(ObjectContainer db) {
      Query query=db.query();
      query.constrain(Event.class);
      ObjectSet result=query.execute();
      while(result.hasNext()) {
          Event e= (Event)result.next();
          Short eventid = e.getEventid();
          String title = e.getTitle();
          String location = e.getLocation();
          Date date = e.getDate();
          Boolean onCampus = e.getOnCampus();
          Short credit= e.getCredit();
          System.out.println(String.format(" Event: %d | %s | %s | %s | %b | %d",eventid,title,location,date.toString(),onCampus,credit));
      }
    }

    private void run() {
        ObjectContainer db = null;
        try {
            /* Open the database. */
            db = Db4o.openFile("ceis.db4o");

            /* Remove all Event objects from the database. */
            clearDatabase(db);

            /* Process the raw data. */
            processInput(db);

            /* Commit all additions. */
            db.commit();

            /* Verify that we have the objects. */
            retrieveAllEvents(db);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Cleanup. */
            if (db != null) db.close();
        }
    }

    public static void main(String[] args) {
        LoadEvents application = new LoadEvents();
        application.run();
    }
}
