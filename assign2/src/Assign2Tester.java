import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Properties;

public class Assign2Tester {
    private void run() throws Exception {
        Properties prop= new Properties();
        prop.load(new FileReader("assign2.properties"));
        int recordSize= new Integer(prop.getProperty("recordsize"));
        int keySize= new Integer(prop.getProperty("keysize"));
        int sampleSize= new Integer(prop.getProperty("samplesize")); // The number of records.

        System.out.println("Begin program.");

        /* Define and initialize the 'seen' array. */
        boolean seen[]= new boolean[sampleSize+1];
        for(int i= 0; i < sampleSize; i += 1)seen[i]= false;

        /* Open the input file. */
        FileInputStream input= new FileInputStream("assign2.sorted.data");

        /* Read the records and test for ascending sequence. */
        byte[] record= new byte[recordSize];
        byte[] key= new byte[keySize];
        byte[] index= new byte[keySize];
        String prevKey= "000000000";
        while(input.read(record) == recordSize){
            for(int i= 0; i < 9; i += 1)key[i]= record[i];
            for(int i= 0; i < 9; i += 1)index[i]= record[44+i];
            String keyStr= new String(key);
            String indexStr= new String(index);
            int cmp= keyStr.compareTo(prevKey);
            if(cmp < 0){
                System.out.printf("Error: %s %s",keyStr,indexStr);
            }
            prevKey= keyStr;

            /* Denote the index as 'seen'. */
            Integer j= new Integer(indexStr);
            seen[j]= true;
        }

        /* Close the input file. */
        input.close();

        /* Have we seen all index values? */
        for(int i= 1; i < sampleSize; i += 1){
            if(!seen[i]){
                System.out.printf("Error: seen[%d]= false.",i);
            }
        }

        System.out.println("End of program.");
    }

    public static void main(String[] args) throws Exception {
        Assign2Tester application = new Assign2Tester();
        application.run();
    }
}
