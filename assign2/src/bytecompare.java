import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: weskinne
 * Date: 2/20/11
 * Time: 3:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class bytecompare {

    public static void main(String args[]) {
        byte[] big = "2739874928374987234".getBytes();
        byte[] small = new byte[3];

        small = big;

        System.out.println(new String(small));



        byte[] greater = "2534".getBytes();
        byte[] less = "2543".getBytes();

        int result = 0;
        long time = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            ByteBuffer gbb = ByteBuffer.wrap(greater);
            ByteBuffer lbb = ByteBuffer.wrap(less);
            result = gbb.compareTo(lbb);
        }


        time = System.currentTimeMillis() - time;

        System.out.println(time);
        System.out.println(result);

        long time2 = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            String gs = new String(greater);
            String ls = new String(less);
            result = gs.compareTo(ls);
        }
        time2 = System.currentTimeMillis() - time;
        System.out.println(time2);
        System.out.println(result);
    }


}
