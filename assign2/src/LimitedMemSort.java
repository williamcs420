import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by IntelliJ IDEA.
 * User: weskinne
 * Date: 2/21/11
 * Time: 12:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class LimitedMemSort {

    private int totalDataSize;
    private int numberOfSectors;
    private int remainderSize;
    public static int keySize;
    private int maxRecordsForHeap;
    private int sampleSize;
    private int recordSize;

    public LimitedMemSort(int sampleSize, int recordSize, int keySize, int maxRecordsForHeap) {
        this.sampleSize = sampleSize;
        this.recordSize = recordSize;
        this.keySize = keySize;
        this.maxRecordsForHeap = maxRecordsForHeap;
    }

    public String sort(String filename) throws Exception {
        totalDataSize = sampleSize * recordSize;

        // if heap is bigger than sample just use one file
        if (maxRecordsForHeap > sampleSize) {
            numberOfSectors = 1;
        } else {
            numberOfSectors = sampleSize / maxRecordsForHeap;
        }

        remainderSize = sampleSize - (maxRecordsForHeap * numberOfSectors);


        FileInputStream fin = new FileInputStream(filename);
        BufferedInputStream in = new BufferedInputStream(fin);

        //determine que size
        int queSize;
        if (maxRecordsForHeap <= sampleSize) {
            queSize = maxRecordsForHeap;
        } else {
            queSize = sampleSize;
        }


        // generate the sectors which are as large as max heap size
        int s = 0;

        long timeToGenerateSectors = System.currentTimeMillis();

        for (s = 0; s < numberOfSectors; s++) {
            PriorityQueue<byte[]> que = new PriorityQueue<byte[]>(queSize, new recordComparator());

            long timeToCreateQue = System.currentTimeMillis();
            // read max heap amount of records from file into 'records'
            for (int r = 0; r < queSize; r++) {
                byte[] record = new byte[recordSize];
                in.read(record);
                que.add(record);
            }
            timeToCreateQue = System.currentTimeMillis() - timeToCreateQue;
            System.out.println("     Time to create que (millis):" + Long.toString(timeToCreateQue));


            // generate a filename for the sector
            String sectorFileName;
            if (numberOfSectors == 1) /* it's sorted */ {
                sectorFileName = "assign2.sorted.data";
            } else {
                sectorFileName = s + ".sec";
            }

            long timeToSaveQuetoDisk = System.currentTimeMillis();

            FileOutputStream out = new FileOutputStream(sectorFileName);
            int rawLength = que.size() * recordSize;
            ByteBuffer rawBuffer = ByteBuffer.allocate(rawLength);
            for (int r = 0; r < rawLength; r += recordSize) {
                rawBuffer.put(que.remove());
            }
            byte[] raw = rawBuffer.array();
            out.write(raw, 0, rawLength);
            timeToSaveQuetoDisk = System.currentTimeMillis() - timeToSaveQuetoDisk;
            System.out.println("     Time to save que to disk (millis):" + Long.toString(timeToSaveQuetoDisk));
        }

        // get the remainder
        PriorityQueue<byte[]> que = new PriorityQueue<byte[]>(queSize, new recordComparator());
        // read remainder amount of records from file into 'records'
        for (int r = 0; r < remainderSize; r++) {
            byte[] record = new byte[recordSize];
            if (in.read(record) > 0)
                que.add(record);
        }

        timeToGenerateSectors = System.currentTimeMillis() - timeToGenerateSectors;
        System.out.println("Time to generate sectors:" + Long.toString(timeToGenerateSectors / 1000));

        // store this sorted remainder list to a sector
        FileOutputStream out = new FileOutputStream(s + ".sec");

        int rawLength = que.size() * recordSize;
        ByteBuffer rawBuffer = ByteBuffer.allocate(rawLength);
        for (int r = 0; r < rawLength; r += recordSize) {
            rawBuffer.put(que.remove());
        }
        byte[] raw = rawBuffer.array();
        out.write(raw, 0, rawLength);

        out.close();

        in.close();

        String sortedFileName = "assign2.sorted.data";

        long timeToMergeSectors = System.currentTimeMillis();

        // now merge the sectors back together
        mergeSectors(sortedFileName);

        timeToMergeSectors = System.currentTimeMillis() - timeToMergeSectors;
        System.out.println("Time to merge sectors:" + Long.toString(timeToMergeSectors / 1000));

        // cleanup
        for (int f = 0; f < numberOfSectors + 1; f++) {
            File file = new File(f + ".sec");
            if (!file.delete()) {
                throw new Exception("Could not delete sector " + f);
            }
        }

        return sortedFileName;
    }

    private void mergeSectors(String sortedFileName) throws Exception {
        /* Ok, so the idea here is going to be to bring one record from each file and
         * save the lowest off to our final sorted file.  When we save a record from a file
         * we need to bring another record in from that file. Also, we will need some kind
         * of intermediary list to keep the records we're pulling in sorted to insure the
         * final file will be sorted. */
        FileOutputStream sortedOut = new FileOutputStream(sortedFileName);
        int numberOfFiles = numberOfSectors + 1;  /* for remainder */

        // TODO: this is what we're going to use for now, we might could get a better time with something custom
        PriorityQueue<FileReadRecord> interQue = new PriorityQueue<FileReadRecord>(numberOfFiles);

        // create the input streams (buffer them so they can utilize the heap)
        int bufferSize = (maxRecordsForHeap * recordSize) / numberOfFiles;
        BufferedInputStream[] inputStreams = new BufferedInputStream[numberOfFiles];
        for (int s = 0; s < numberOfFiles; s++) {
            FileInputStream in = new FileInputStream(s + ".sec");
            inputStreams[s] = new BufferedInputStream(in, bufferSize);

        }

        // set the queue up
        for (int f = 0; f < numberOfFiles; f++) {
            byte[] record = new byte[recordSize];
            inputStreams[f].read(record);
            FileReadRecord frrecord = new FileReadRecord(record, f);
            interQue.add(frrecord);
        }


        while (!interQue.isEmpty()) {
            // get the lowest element in que and save it to sorted file
            FileReadRecord lowest = interQue.remove();
            sortedOut.write(lowest.record);

            // replace lowest with new record from same file
            byte[] record = new byte[recordSize];
            if (inputStreams[lowest.fileId].read(record) > 0) {
                FileReadRecord replacement = new FileReadRecord(record, lowest.fileId);
                interQue.add(replacement);
            }
        }

        for (BufferedInputStream bufferedInputStream : inputStreams) {
            bufferedInputStream.close();
        }

        sortedOut.close();

    }

    private void sortRecords(byte[][] records) {

    }
}

class FileReadRecord implements Comparable {
    public int fileId;
    byte[] record;

    public FileReadRecord(byte[] record, int fileid) {
        this.record = record;
        this.fileId = fileid;
    }

    public int compareTo(FileReadRecord that) {

        byte[] key1 = new byte[LimitedMemSort.keySize];
        System.arraycopy(record, 0, key1, 0, LimitedMemSort.keySize);

        byte[] key2 = new byte[LimitedMemSort.keySize];
        System.arraycopy(that.record, 0, key2, 0, LimitedMemSort.keySize);

        return ByteBuffer.wrap(key1).compareTo(ByteBuffer.wrap(key2));
    }

    public int compareTo(Object o) {
        if (o.getClass() == FileReadRecord.class) {
            return compareTo((FileReadRecord) o);
        }

        return 0;
    }
}

class recordComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        byte[] rec1 = (byte[]) o1;
        byte[] rec2 = (byte[]) o2;

        byte[] key1 = new byte[LimitedMemSort.keySize];
        System.arraycopy(rec1, 0, key1, 0, LimitedMemSort.keySize);

        byte[] key2 = new byte[LimitedMemSort.keySize];
        System.arraycopy(rec2, 0, key2, 0, LimitedMemSort.keySize);

        return ByteBuffer.wrap(key1).compareTo(ByteBuffer.wrap(key2));
    }
}
