import java.io.FileReader;
import java.util.Properties;

public class Assign2 {

    private static Integer recordSize;
    private static Integer keySize;
    private static Integer sampleSize;
    private static int sectorSize;


    public static void main(String args[]) throws Exception {
        Properties prop = new Properties();
        prop.load(new FileReader("assign2.properties"));
        recordSize = new Integer(prop.getProperty("recordsize"));
        keySize = new Integer(prop.getProperty("keysize"));
        sampleSize = new Integer(prop.getProperty("samplesize")); // The number of records.
        sectorSize = sampleSize / 4;

        int HeapSizeInMegs = 24;
        int HeapSizeInBytes = HeapSizeInMegs * 100000; /* we can just use a 2.4 mb heap, the time doesn't change */

        int maxRecordsForHeap = HeapSizeInBytes / recordSize;

        LimitedMemSort memSort = new LimitedMemSort(sampleSize, recordSize, keySize, maxRecordsForHeap);

        long startTime = System.currentTimeMillis();

        memSort.sort("assign2.data");

        long totalTime = (System.currentTimeMillis()-startTime)/1000;
        System.out.println(String.format("Total time: %s",Long.toString(totalTime)));


    }
}