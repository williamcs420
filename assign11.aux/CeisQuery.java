import com.sleepycat.db.Environment;
import com.sleepycat.db.EnvironmentConfig;
import com.sleepycat.dbxml.*;

import java.io.File;

public class CeisQuery {

    private void run() {
        Environment myEnv;
        File envHome = new File("xmlEnvironment");
        XmlManager myManager = null;
        XmlContainer myContainer = null;

        try {
            /* Create the environment. */
            EnvironmentConfig envConf = new EnvironmentConfig();
            envConf.setAllowCreate(false);
            envConf.setInitializeCache(true);
            myEnv = new Environment(envHome, envConf);

            /* Create the manager. */
            XmlManagerConfig managerConfig = new XmlManagerConfig();
            managerConfig.setAdoptEnvironment(true);
            myManager = new XmlManager(myEnv, managerConfig);

            /* Open the container. */
            myContainer = myManager.openContainer("ceis.dbxml");

            // Get a query context
            XmlQueryContext context = myManager.createQueryContext();
            context.setReturnType(XmlQueryContext.LiveValues);
            context.setEvaluationType(XmlQueryContext.Eager);

            // Declare the query string
            String myQuery = "for $s in doc(\"dbxml:ceis.dbxml/ceis.xml\")/ceis/students/student return <studentid>{$s/studentid/string()}</studentid>";
            XmlQueryExpression qe = myManager.prepare(myQuery, context);
            /* Execute the query. */
            XmlResults results = qe.execute(context);

            /* Display the results. */
            System.out.printf("Count=%d%n", results.size());
            XmlValue value = results.next();
            while (value != null) {
                /* Get the xml returned by query. */
                String result = value.asString();

                /* Debug output. */
                System.out.printf("result= %s%n", result);

                /* Get the next value returned by the query. */
                value = results.next();
            }

            /* Cleanup. */
            results.delete();
            qe.delete();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Close the container and the manager (manager closes environment). */
            try {
                if (myContainer != null) {
                    myContainer.close();
                    System.out.println("Container closed.");
                }
                if (myManager != null) {
                    myManager.close();
                    System.out.println("Manager closed.");
                }
                System.out.println("End of program.");
            } catch (XmlException ce) {
                ce.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        CeisQuery application = new CeisQuery();
        application.run();
    }
}
