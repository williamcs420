import java.util.Random;
import java.util.Properties;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class DataGenerator {

    private void run() throws Exception {
        Properties prop= new Properties();
        prop.load(new FileReader("assign3.properties"));

        final int sampleSize= new Integer(prop.getProperty("samplesize")); // The number of records.
        Random rand= new Random(1241L);

        /* Open the output data file. */
        FileOutputStream output= new FileOutputStream("assign3.data");

        /* Generate the required number of records. */
        for(int index= 1; index <= sampleSize; index += 1){
            /* The key is a random 9 digit value. */
            int id= rand.nextInt(100000000)+100000000;
            /* Format the key and value (55 bytes) into a String. */
            String data= String.format("%09dBegin data for id=%09d; index=%09d. **** END\n",id,id,index);
            /* Write the bytes that make-up the value (64 bytes). */
            output.write(data.getBytes());
        }
        /* Close the file. */
        output.close();
    }

    public static void main(String[] args) throws Exception {
        DataGenerator application= new DataGenerator();
        application.run();
    }
}
