import java.io.IOException;

/* An interface describing a persistent storage manager which stores key-value pairs. */
public interface StorageManagement {

    /* The 'add' method returns true if the addition was successful; duplicate keys are not permitted. */
    public boolean add(byte[] key, byte[] value) throws IOException;

    /* The 'find' method returns the value array associated with a key or null if the key is not found. */
    public byte[] find(byte[] key) throws Exception;
}
