import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 2/28/11
 * Time: 1:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class FCTest {
    public static void main(String args[]) throws Exception {
        FileChannel fc = new FileOutputStream("testing.txt").getChannel();

        byte[] str = "testing".getBytes();

        fc.write(ByteBuffer.wrap(str));

        fc.write(ByteBuffer.wrap(str),89);

        fc.close();
    }
}
