public class Node {
    private byte[] key;
    private byte[] value;

    private Node leftChild;
    private Node rightChild;

    Node(byte[] key, byte[] value) {
        this.key = key.clone();
        this.value = value.clone();
        leftChild = null;
        rightChild = null;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }
}

