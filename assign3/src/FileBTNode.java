import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

public class FileBTNode implements Comparable {
    public byte[] key;
    public byte[] value;
    public long leftChildPosition;
    public long rightChildPosition;


    public FileBTNode(byte[] key, byte[] record) {
        this.key = key.clone();
        this.value = record.clone();
        this.leftChildPosition = 0L;
        this.rightChildPosition = 0L;
    }

    public int compareTo(Object o) {
        FileBTNode that = (FileBTNode) o;

        return ByteBuffer.wrap(this.key).compareTo(ByteBuffer.wrap(that.key));
    }

    public static void write(FileBTNode newNode, RandomAccessFile file, long filePosition) throws Exception {
        long originalPosition = file.getFilePointer();
        file.seek(filePosition);

        file.write(newNode.key);
        file.write(newNode.value);
        file.writeLong(newNode.leftChildPosition);
        file.writeLong(newNode.rightChildPosition);

        StorageManager.trueOffset += StorageManager.dbRecordSize;
        file.seek(originalPosition);
    }
}
