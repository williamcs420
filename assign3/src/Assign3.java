import java.io.*;
import java.util.Properties;

public class Assign3 {
    private void run() throws Exception {
        Properties prop= new Properties();
        prop.load(new FileReader("assign3.properties"));
        final int recordSize= new Integer(prop.getProperty("recordsize"));
        final int keySize= new Integer(prop.getProperty("keysize"));

        /* Create a StorageManager. */
        StorageManager sm = new StorageManager("assign3.db",recordSize,keySize);

        /* Read the input records from assign3.data and save them. */
        FileInputStream input= new FileInputStream("assign3.data");
        byte[] record= new byte[recordSize];
        byte[] key= new byte[keySize];
        byte[] value= new byte[recordSize-keySize];
        System.out.println("Begin loading.");
        long startTime= System.currentTimeMillis();
        while(input.read(record) == recordSize){
            System.arraycopy(record,0,key,0,keySize);
            System.arraycopy(record,keySize,value,0,recordSize-keySize);
            sm.add(key,value);
        }
        sm.dump();
        long endTime= System.currentTimeMillis();
        input.close();
        System.out.printf("End of loading; time=%d seconds.%n",(endTime-startTime)/1000);


        /* OK... now read the same records and try to find each key. */
        input= new FileInputStream("assign3.data");
        System.out.println("Starting search.");
        startTime= System.currentTimeMillis();
        while(input.read(record) == recordSize){
            System.arraycopy(record,0,key,0,keySize);
            value= sm.find(key);
            /* Did we find the associated value? */
            if(value == null){
                /* No!!! */
                System.out.printf("Error while processing %s.%n",new String(key));
            }
        }
        endTime= System.currentTimeMillis();
        input.close();
        System.out.printf("End of searching; time=%d seconds.%n",(endTime-startTime)/1000);

        /* Close the storage. */
        sm.close();
    }

    private void init() throws Exception {
        /* Open the 'storage' file. */
        RandomAccessFile storage = new RandomAccessFile("assign3.db", "rw");

        /* Force the file length to zero. */
        storage.setLength(0L);

        /* Close the storage file. */
        storage.close();
    }

    public static void main(String[] args) throws Exception {
        Assign3 application = new Assign3();
        application.init();
        application.run();
    }
}
