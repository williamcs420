import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class StorageManager implements StorageManagement {
    private RandomAccessFile dbfile;
    static int recordSize;
    static int keySize;
    static int valueSize;
    static long trueOffset;
    static int dbRecordSize;

    private Node root;

    public StorageManager(String outputFileName, int recordSize, int keySize) throws Exception {
        dbfile = new RandomAccessFile(outputFileName, "rw");
        this.recordSize = recordSize;
        this.keySize = keySize;
        this.valueSize = recordSize - keySize;
        dbRecordSize = recordSize + 8 + 8;
        root = null;
        trueOffset = 0L;
    }

    public boolean add(byte[] key, byte[] value) throws IOException {
        FileBTNode newNode = new FileBTNode(key, value);
        if (trueOffset > 0) {
            try {
                insert(0, newNode);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return false;
            }
        } else {
            // first record
            try {
                writeNode(newNode, trueOffset);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return false;
            }
        }

        return true;
    }

    private void writeNode(FileBTNode newNode, long trueOffset) throws Exception {
        FileBTNode.write(newNode, dbfile, trueOffset);
    }

    public boolean insert(long filePosition, FileBTNode newNode) throws Exception {
        byte[] currKey = new byte[keySize];
        dbfile.seek(filePosition);
        dbfile.read(currKey);

        int compare = ByteBuffer.wrap(newNode.key).compareTo(ByteBuffer.wrap(currKey));

        if (compare > 0) {
            dbfile.seek(filePosition + recordSize + 8);
            long rightPos = dbfile.readLong();

            // see if there is a right child
            if (rightPos != 0) {
                // traverse to that child
                insert(rightPos, newNode);
            } else {
                // setup this node as the right child
                dbfile.seek(filePosition + recordSize + 8);
                dbfile.writeLong(trueOffset);

                // write the node
                FileBTNode.write(newNode, dbfile, trueOffset);

                return true;
            }
        } else if (compare < 0) {
            dbfile.seek(filePosition + recordSize);
            long leftPos = dbfile.readLong();

            // see if there is a right child
            if (leftPos != 0) {
                // traverse to that child
                insert(leftPos, newNode);
            } else {
                // setup this node as the left child
                dbfile.seek(filePosition + recordSize);
                dbfile.writeLong(trueOffset);

                // write the node
                FileBTNode.write(newNode, dbfile, trueOffset);

                return true;
            }
        }

        return false;
    }


    public byte[] find(byte[] key) throws Exception {
        byte[] value = find(0L, ByteBuffer.wrap(key));

        return value;
    }

    private byte[] find(long filePosition, ByteBuffer key) throws Exception {
        byte[] currKey = new byte[keySize];
        dbfile.seek(filePosition);
        dbfile.read(currKey);

        int compare = key.compareTo(ByteBuffer.wrap(currKey));
        byte[] value = new byte[valueSize];
        if (compare > 0) {
            dbfile.seek(filePosition+recordSize+8);
            long rightPos = dbfile.readLong();

            // see if there is a right child
            if (rightPos != 0) {
                // traverse to that child
                find(rightPos, key);
            } else {
                return null;
            }
        } else if (compare < 0) {
            dbfile.seek(filePosition+recordSize+8);
            long leftPos = dbfile.readLong();

            // see if there is a left child
            if (leftPos != 0) {
                // traverse to that child
                find(leftPos, key);
            } else {
                return null;
            }
        } else {
            dbfile.read(value);
        }
        return value;
    }


    public void dump() throws Exception {

    }


    public void close() throws IOException {
        dbfile.close();
    }
}

