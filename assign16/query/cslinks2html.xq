<html>
    <body>
        <table border="1">
            <thead>
                <td>Description</td>
                <td>Link</td>
                <td>Sortkey</td>
            </thead>

        {
        for $t in //linkdata
            where $t/keyword = "xml"
            order by $t/@sortkey
            return <tr>

                    <td>{ $t/description/text() }</td>
                    <td>{ $t/link/text() }</td>
                    <td>{ data($t/@sortkey) }</td>

                </tr>
        }
        </table>
    </body>
</html>

