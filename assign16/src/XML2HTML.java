import net.sf.saxon.Configuration;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.query.StaticQueryContext;
import net.sf.saxon.query.XQueryExpression;
import net.sf.saxon.query.DynamicQueryContext;

import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class XML2HTML {

    private void run() throws IOException, XPathException {
        /* Setup for XQuery processing. */
        Configuration config = new Configuration();
        StaticQueryContext sqc = config.newStaticQueryContext();

        /* Compile the XQuery input. */
        XQueryExpression exp = sqc.compileQuery(new FileReader("query/cslinks2html.xq"));

        /* Load the XML data file. */
        DynamicQueryContext dynamicContext = new DynamicQueryContext(config);
        dynamicContext.setContextItem(config.buildDocument(new StreamSource("data/cslinks.xml")));

        /* Use the compiled XQuery to convert the XML to HTML. */
        Properties props = new Properties();
        props.setProperty(OutputKeys.METHOD, "html");
        props.setProperty(OutputKeys.DOCTYPE_PUBLIC, "-//W3C//DTD HTML 4.01 Transitional//EN");
        exp.run(dynamicContext, new StreamResult(new File("cslinks.html")), props);
    }

    public static void main(String[] args) throws IOException, XPathException {
        XML2HTML application= new XML2HTML();
        application.run();
    }
}
