<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.sql.*" %>
<html>
<head>
    <title>Assign5: Sales Order Addition Processing</title>
</head>
<body>
<center>
    <h1>Assign5: Sales Order Addition Processing</h1>
    <%
        String statusMsg = "";
        boolean debug = true;
        Connection con = null;
        Statement stmnt = null;

        /* Get all incoming parameters into the 'parms' Hashtable. */
        Enumeration keys = request.getParameterNames();
        Hashtable<String, String> parms = new Hashtable<String, String>();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String value = request.getParameter(key);
            if (debug) System.out.println(String.format("key=%s value=%s", key, value));
            parms.put(key, value);
        }

        try {
            /* Load the database driver. */
            Class.forName("org.postgresql.Driver");

            /* Establish a connection to database. */
            con = DriverManager.getConnection("jdbc:postgresql://localhost/weskinne", "weskinne", "wes7894");

            /* Turn off autocommit. */
            con.setAutoCommit(false);

            /* Create a statement object by which SQL commands may be issued. */
            stmnt = con.createStatement();

            /* ???? */
            int sonum = Integer.parseInt(parms.get("sonum"));
            int stor_id = Integer.parseInt(parms.get("stor_id"));
            String ponum = (String) parms.get("ponum");
            int qty_ordered = Integer.parseInt(parms.get("qty_ordered"));
            String title_id = (String) parms.get("title_id");

            String salesSql = String.format("INSERT INTO sales (sonum,stor_id,ponum) VALUES ('%s','%s','%s')", sonum,stor_id, ponum);

            stmnt.execute(salesSql);

            stmnt.close();

            stmnt = con.createStatement();

            String detailsSql = String.format("INSERT INTO salesdetails (sonum,qty_ordered,title_id) VALUES ('%s','%s','%s')",
                    sonum,qty_ordered,title_id);

            try {
                stmnt.execute(detailsSql);
            }
            catch (SQLException e) {
                // if there was a database error, remove the sales record
                String salesDeleteSql = String.format("DELETE FROM sales WHERE sonum='%s'",sonum);
                stmnt.execute(salesDeleteSql);

                throw e;
            }
            statusMsg += "Sale Processed";

        } catch (Exception e) {
            statusMsg += String.format("Error: %s<br>", e.getMessage());
        } finally {
        }
        if (debug) System.out.println("statusMsg=" + statusMsg);
    %>
    <p><%= statusMsg%>
    </p>


</center>
</body>
</html>