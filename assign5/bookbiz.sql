
CREATE TABLE authors (
    au_id character(11) primary key,
    au_lname character varying(40) NOT NULL,
    au_fname character varying(20) NOT NULL,
    phone character(12),
    address character varying(40),
    city character varying(20),
    state character(2),
    zip character(5)
);


CREATE TABLE publishers (
    pub_id character(4) primary key,
    pub_name character varying(40) not null,
    address character varying(40) not null,
    city character varying(20) not null,
    state character(2) not null
);


CREATE TABLE roysched (
    title_id character(6),
    lorange integer,
    hirange integer,
    royalty double precision
);


CREATE TABLE titleauthors (
    au_id character(11) NOT NULL,
    title_id character(6) NOT NULL,
    au_ord smallint,
    royaltyshare double precision,
    primary key(au_id,title_id)
);


CREATE TABLE titles (
    title_id character(6) primary key,
    title character varying(80) NOT NULL,
    "type" character(12),
    pub_id character(4),
    price double precision,
    advance double precision,
    ytd_sales integer,
    contract boolean NOT NULL,
    notes character varying(200),
    pubdate date
);


CREATE TABLE editors (
    ed_id character(11) primary key,
    ed_lname character varying(40) NOT NULL,
    ed_fname character varying(20) NOT NULL,
    ed_pos character varying(12),
    phone character(12),
    address character varying(40),
    city character varying(20),
    state character(2),
    zip character(5)
);


CREATE TABLE titleditors (
    ed_id character(11) NOT NULL,
    title_id character(6) NOT NULL,
    ed_ord smallint,
    primary key(ed_id,title_id)
);


CREATE TABLE sales (
    sonum integer primary key,
    stor_id character(4) NOT NULL,
    ponum character varying(20) NOT NULL,
    sdate date
);


CREATE TABLE salesdetails (
    sonum integer NOT NULL,
    qty_ordered smallint NOT NULL,
    qty_shipped smallint,
    title_id character(6) NOT NULL,
    date_shipped date,
    primary key(sonum,title_id)
);



CREATE VIEW titleview AS
    SELECT titles.title, titleauthors.au_ord, authors.au_lname, titles.price, titles.ytd_sales, titles.pub_id FROM authors, titles, titleauthors WHERE ((authors.au_id = titleauthors.au_id) AND (titles.title_id = titleauthors.title_id));



CREATE VIEW oaklanders AS
    SELECT authors.au_fname, authors.au_lname, titles.title FROM authors, titles, titleauthors WHERE (((authors.au_id = titleauthors.au_id) AND (titles.title_id = titleauthors.title_id)) AND ((authors.city)::text = 'Oakland'::text));



CREATE VIEW books AS
    SELECT titles.title_id, titleauthors.au_ord, authors.au_lname, authors.au_fname FROM authors, titles, titleauthors WHERE ((authors.au_id = titleauthors.au_id) AND (titles.title_id = titleauthors.title_id));



CREATE VIEW royaltychecks AS
    SELECT authors.au_lname, authors.au_fname, sum((((titles.price * (titles.ytd_sales)::double precision) * roysched.royalty) * titleauthors.royaltyshare)) AS total_income FROM authors, titles, titleauthors, roysched WHERE ((((authors.au_id = titleauthors.au_id) AND (titles.title_id = titleauthors.title_id)) AND (titles.title_id = roysched.title_id)) AND ((titles.ytd_sales >= roysched.lorange) AND (titles.ytd_sales <= roysched.hirange))) GROUP BY authors.au_lname, authors.au_fname;



CREATE VIEW currentinfo AS
    SELECT titles.pub_id AS pubid, titles."type", sum((titles.price * (titles.ytd_sales)::double precision)) AS income, avg(titles.price) AS avg_price, avg(titles.ytd_sales) AS avg_sales FROM titles GROUP BY titles.pub_id, titles."type";


CREATE VIEW hiprice AS
    SELECT titles.title_id, titles.title, titles."type", titles.pub_id, titles.price, titles.advance, titles.ytd_sales, titles.contract, titles.notes, titles.pubdate FROM titles WHERE ((titles.price > (15)::double precision) AND (titles.advance > (5000)::double precision));


CREATE VIEW cities AS
    SELECT authors.au_lname AS author, authors.city AS authorcity, publishers.pub_name AS pub, publishers.city AS pubcity FROM authors, publishers WHERE ((authors.city)::text = (publishers.city)::text);


CREATE VIEW highaverage AS
    SELECT authors.au_id, titles.title_id, publishers.pub_name, titles.price FROM authors, titleauthors, titles, publishers WHERE ((((authors.au_id = titleauthors.au_id) AND (titles.title_id = titleauthors.title_id)) AND (titles.pub_id = publishers.pub_id)) AND (titles.price > (SELECT avg(titles.price) AS avg FROM titles)));


CREATE VIEW highbandh AS
    SELECT highaverage.au_id, highaverage.title_id, highaverage.pub_name, highaverage.price FROM highaverage WHERE ((highaverage.pub_name)::text = 'Binnet & Hardley'::text);


CREATE VIEW number1 AS
    SELECT authors.au_lname, authors.phone FROM authors WHERE (authors.zip ~~ '94%'::text);


CREATE VIEW number2 AS
    SELECT number1.au_lname, number1.phone FROM number1 WHERE ((number1.au_lname)::text ~~ '[M-Z]%'::text);


CREATE VIEW number3 AS
    SELECT number2.au_lname, number2.phone FROM number2 WHERE ((number2.au_lname)::text = 'MacFeather'::text);


CREATE VIEW accounts AS
    SELECT titles.title_id AS title, titles.advance, (titles.price * (titles.ytd_sales)::double precision) AS gross_sales FROM titles WHERE ((titles.price > (15)::double precision) AND (titles.advance > (5000)::double precision));


CREATE VIEW categories AS
    SELECT titles."type" AS category, avg(titles.price) AS average_price FROM titles GROUP BY titles."type";


--
-- Data for TOC entry 47 (OID 17651)
-- Name: authors; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY authors (au_id, au_lname, au_fname, phone, address, city, state, zip) FROM stdin;
409-56-7008	Bennet	Abraham	415 658-9932	6223 Bateman St.	Berkeley	CA	94705
213-46-8915	Green	Marjorie	415 986-7020	309 63rd St. #411	Oakland	CA	94618
238-95-7766	Carson	Cheryl	415 548-7723	589 Darwin Ln.	Berkeley	CA	94705
998-72-3567	Ringer	Albert	801 826-0752	67 Seventh Av.	Salt Lake City	UT	84152
899-46-2035	Ringer	Anne	801 826-0752	67 Seventh Av.	Salt Lake City	UT	84152
722-51-5454	DeFrance	Michel	219 547-9982	3 Balding Pl.	Gary	IN	46403
807-91-6654	Panteley	Sylvia	301 946-8853	1956 Arlington Pl.	Rockville	MD	20853
893-72-1158	McBadden	Heather	707 448-4982	301 Putnam	Vacaville	CA	95688
724-08-9931	Stringer	Dirk	415 843-2991	5420 Telegraph Av.	Oakland	CA	94609
274-80-9391	Straight	Dick	415 834-2919	5420 College Av.	Oakland	CA	94609
756-30-7391	Karsen	Livia	415 534-9219	5720 McAuley St.	Oakland	CA	94609
724-80-9391	MacFeather	Stearns	415 354-7128	44 Upland Hts.	Oakland	CA	94612
427-17-2319	Dull	Ann	415 836-7128	3410 Blonde St.	Palo Alto	CA	94301
672-71-3249	Yokomoto	Akiko	415 935-4228	3 Silver Ct.	Walnut Creek	CA	94595
267-41-2394	O'Leary	Michael	408 286-2428	22 Cleveland Av. #14	San Jose	CA	95128
472-27-2349	Gringlesby	Burt	707 938-6445	PO Box 792	Covelo	CA	95428
527-72-3246	Greene	Morningstar	615 297-2723	22 Graybar House Rd.	Nashville	TN	37215
172-32-1176	White	Johnson	408 496-7223	10932 Bigge Rd.	Menlo Park	CA	94025
712-45-1867	del Castillo	Innes	615 996-8275	2286 Cram Pl. #86	Ann Arbor	MI	48105
846-92-7186	Hunter	Sheryl	415 836-7128	3410 Blonde St.	Palo Alto	CA	94301
486-29-1786	Locksley	Chastity	415 585-4620	18 Broadway Av.	San Francisco	CA	94130
648-92-1872	Blotchet-Halls	Reginald	503 745-6402	55 Hillsdale Bl.	Corvallis	OR	97330
341-22-1782	Smith	Meander	913 843-0462	10 Misisipi Dr.	Lawrence	KS	66044
\.


--
-- Data for TOC entry 48 (OID 17653)
-- Name: publishers; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY publishers (pub_id, pub_name, address, city, state) FROM stdin;
0736	New Age Books	1 1st St	Boston	MA
0877	Binnet & Hardley	2 2nd Ave.	Washington	DC
1389	Algodata Infosystems	3 3rd Dr.	Berkeley	CA
\.


--
-- Data for TOC entry 49 (OID 17655)
-- Name: roysched; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY roysched (title_id, lorange, hirange, royalty) FROM stdin;
BU1032	0	5000	0.10000000000000001
PC1035	0	2000	0.10000000000000001
PC1035	2001	4000	0.12
PC1035	4001	50000	0.16
BU2075	0	1000	0.10000000000000001
BU2075	1001	5000	0.12
BU2075	5001	7000	0.16
BU2075	7001	50000	0.17999999999999999
PS9999	0	50000	0.10000000000000001
PS2091	0	1000	0.10000000000000001
PS2091	1001	5000	0.12
PS2091	5001	50000	0.14000000000000001
PS2106	0	2000	0.10000000000000001
PS2106	2001	5000	0.12
PS2106	5001	50000	0.14000000000000001
MC3021	0	1000	0.10000000000000001
MC3021	1001	2000	0.12
MC3021	2001	6000	0.14000000000000001
MC3021	6001	8000	0.17999999999999999
MC3021	8001	50000	0.20000000000000001
TC3218	0	2000	0.10000000000000001
TC3218	2001	6000	0.12
TC3218	6001	8000	0.16
TC3218	8001	50000	0.16
PC8888	0	5000	0.10000000000000001
PC8888	5001	50000	0.12
PS7777	0	5000	0.10000000000000001
PS7777	5001	50000	0.12
PS3333	0	5000	0.10000000000000001
PS3333	5001	50000	0.12
MC3026	0	1000	0.10000000000000001
MC3026	1001	2000	0.12
MC3026	2001	6000	0.14000000000000001
MC3026	6001	8000	0.17999999999999999
MC3026	8001	50000	0.20000000000000001
BU1111	0	4000	0.10000000000000001
BU1111	4001	8000	0.12
BU1111	8001	50000	0.14000000000000001
MC2222	0	2000	0.10000000000000001
MC2222	2001	4000	0.12
MC2222	4001	8000	0.14000000000000001
MC2222	8001	12000	0.16
TC7777	0	5000	0.10000000000000001
TC7777	5001	15000	0.12
TC4203	0	2000	0.10000000000000001
TC4203	2001	8000	0.12
TC4203	8001	16000	0.14000000000000001
BU7832	0	5000	0.10000000000000001
BU7832	5001	50000	0.12
PS1372	0	50000	0.10000000000000001
\.


--
-- Data for TOC entry 50 (OID 17657)
-- Name: titleauthors; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY titleauthors (au_id, title_id, au_ord, royaltyshare) FROM stdin;
409-56-7008	BU1032	1	0.59999999999999998
486-29-1786	PS7777	1	1
486-29-1786	PC9999	1	1
712-45-1867	MC2222	1	1
172-32-1176	PS3333	1	1
213-46-8915	BU1032	2	0.40000000000000002
238-95-7766	PC1035	1	1
213-46-8915	BU2075	1	1
998-72-3567	PS2091	1	0.5
899-46-2035	PS2091	2	0.5
998-72-3567	PS2106	1	1
722-51-5454	MC3021	1	0.75
899-46-2035	MC3021	2	0.25
807-91-6654	TC3218	1	1
274-80-9391	BU7832	1	1
427-17-2319	PC8888	1	0.5
846-92-7186	PC8888	2	0.5
756-30-7391	PS1372	1	0.75
724-80-9391	PS1372	2	0.25
724-80-9391	BU1111	1	0.59999999999999998
267-41-2394	BU1111	2	0.40000000000000002
672-71-3249	TC7777	1	0.40000000000000002
267-41-2394	TC7777	2	0.29999999999999999
472-27-2349	TC7777	3	0.29999999999999999
648-92-1872	TC4203	1	1
\.


--
-- Data for TOC entry 51 (OID 17659)
-- Name: titles; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY titles (title_id, title, "type", pub_id, price, advance, ytd_sales, contract, notes, pubdate) FROM stdin;
PC8888	Secrets of Silicon Valley	popular_comp	1389	20	8000	4095	t	Muckraking reporting on the world's largest computer hardware and software manufacturers.	1985-06-12
BU1032	The Busy Executive's Database Guide	business    	1389	19.989999999999998	5000	4095	t	An overview of available database systems with emphasis on common business applications.  Illustrated.	1985-06-12
PS7777	Emotional Security: A New Algorithm	psychology  	0736	7.9900000000000002	4000	3336	t	Protecting yourself and your loved ones from undue emotional stress in the modern world.  Use of computer and nutritional aids emphasized.	1985-06-12
PS3333	Prolonged Data Deprivation: Four Case Studies	psychology  	0736	19.989999999999998	2000	4072	t	What happens when the data runs dry?  Searching evaluations of information-shortage effects.	1985-06-12
BU1111	Cooking with Computers: Surreptitious Balance Sheets	business    	1389	11.949999999999999	5000	3876	t	Helpful hints on how to use your electronic resources to the best advantage.	1985-06-09
MC2222	Silicon Valley Gastronomic Treats	mod_cook    	0877	19.989999999999998	0	2032	t	Favorite recipes for quick, easy, and elegant meals tried and tested by people who never have time to eat, let alone cook.	1985-06-09
TC7777	Sushi, Anyone?	trad_cook   	0877	14.99	8000	4095	t	Detailed instructions on improving your position in life by learning how to make authentic Japanese sushi in your spare time. 5-10% increase in number of friends per recipe reported from beta test. 	1985-06-12
TC4203	Fifty Years in Buckingham Palace Kitchens	trad_cook   	0877	11.949999999999999	4000	15096	t	More anecdotes from the Queen's favorite cook describing life among English royalty.  Recipes, techniques, tender vignettes.	1985-06-12
PC1035	But Is It User Friendly?	popular_comp	1389	22.949999999999999	7000	8780	t	A survey of software for the naive user, focusing on the 'friendliness' of each.	1985-06-30
BU2075	You Can Combat Computer Stress!	business    	0736	2.9900000000000002	10125	18722	t	The latest medical and psychological techniques for living with the electronic office.  Easy-to-understand explanations.	1985-06-30
PS2091	Is Anger the Enemy?	psychology  	0736	10.949999999999999	2275	2045	t	Carefully researched study of the effects of strong emotions on the body. Metabolic charts included.	1985-06-15
PS2106	Life Without Fear	psychology  	0736	7	6000	111	t	New exercise, meditation, and nutritional techniques that can reduce the shock of daily interactions. Popular audience.  Sample menus included,exercise video available separately.	1985-10-05
MC3021	The Gourmet Microwave	mod_cook    	0877	2.9900000000000002	15000	22246	t	Traditional French gourmet recipes adapted for modern microwave cooking.	1985-06-18
TC3218	Onions, Leeks, and Garlic: Cooking Secrets of the Mediterranean	trad_cook   	0877	20.949999999999999	7000	375	t	Profusely illustrated in color, this makes a wonderful gift book for a cuisine-oriented friend.	1985-10-21
MC3026	The Psychology of Computer Cooking	\N	0877	\N	\N	\N	f	\N	\N
BU7832	Straight Talk About Computers	business    	1389	19.989999999999998	5000	4095	t	Annotated analysis of what computers can do for you: a no-hype guide for the critical user.	1985-06-22
PS1372	Computer Phobic and Non-Phobic Individuals: Behavior Variations	psychology  	0736	21.59	7000	375	t	A must for the specialist, this book examines the difference between those who hate and fear computers and those who think they are swell.	1985-10-21
PC9999	Net Etiquette	popular_comp	1389	\N	\N	\N	f	A must-read for computer conferencing debutantes!.	\N
\.


--
-- Data for TOC entry 52 (OID 17661)
-- Name: editors; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY editors (ed_id, ed_lname, ed_fname, ed_pos, phone, address, city, state, zip) FROM stdin;
321-55-8906	DeLongue	Martinella	project	415 843-2222	3000 6th St.	Berkeley	CA	94710
723-48-9010	Sparks	Manfred	copy	303 721-3388	15 Sail	Denver	CO	80237
777-02-9831	Samuelson	Bernard	project	415 843-6990	27 Yosemite	Oakland	CA	94609
777-66-9902	Almond	Alfred	copy	312 699-4177	1010 E. Devon	Chicago	IL	60018
826-11-9034	Himmel	Eleanore	project	617 423-0552	97 Bleaker	Boston	MA	02210
885-23-9140	Rutherford-Hayes	Hannah	project	301 468-3909	32 Rockbill Pike	Rockbill	MD	20852
993-86-0420	McCann	Dennis	acquisition	301 468-3909	32 Rockbill Pike	Rockbill	MD	20852
943-88-7920	Kaspchek	Christof	acquisition	415 549-3909	18 Severe Rd.	Berkeley	CA	94710
234-88-9720	Hunter	Amanda	acquisition	617 432-5586	18 Dowdy Ln.	Boston	MA	02210
\.


--
-- Data for TOC entry 53 (OID 17663)
-- Name: titleditors; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY titleditors (ed_id, title_id, ed_ord) FROM stdin;
826-11-9034	BU2075	2
826-11-9034	PS2091	2
826-11-9034	PS2106	2
826-11-9034	PS3333	2
826-11-9034	PS7777	2
826-11-9034	PS1372	2
885-23-9140	MC2222	2
885-23-9140	MC3021	2
885-23-9140	TC3281	2
885-23-9140	TC4203	2
885-23-9140	TC7777	2
321-55-8906	BU1032	2
321-55-8906	BU1111	2
321-55-8906	BU7832	2
321-55-8906	PC1035	2
321-55-8906	PC8888	2
321-55-8906	BU2075	3
777-02-9831	PC1035	3
777-02-9831	PC8888	3
943-88-7920	BU1032	1
943-88-7920	BU1111	1
943-88-7920	BU2075	1
943-88-7920	BU7832	1
943-88-7920	PC1035	1
943-88-7920	PC8888	1
993-86-0420	PS1372	1
993-86-0420	PS2091	1
993-86-0420	PS2106	1
993-86-0420	PS3333	1
993-86-0420	PS7777	1
993-86-0420	MC2222	1
993-86-0420	MC3021	1
993-86-0420	TC3218	1
993-86-0420	TC4203	1
993-86-0420	TC7777	1
\.


--
-- Data for TOC entry 54 (OID 17665)
-- Name: sales; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY sales (sonum, stor_id, ponum, sdate) FROM stdin;
1	7066	QA7442.3	1985-09-13
2	7067	D4482	1985-09-14
3	7131	N914008	1985-09-14
4	7131	N914014	1985-09-14
5	8042	423LL922	1985-09-14
6	8042	423LL930	1985-09-14
7	6380	722a	1985-09-13
8	6380	6871	1985-09-14
9	8042	P723	1988-03-11
19	7896	X999	1988-02-21
10	7896	QQ2299	1987-10-28
11	7896	TQ456	1987-12-12
12	8042	QA879.1	1987-05-22
13	7066	A2976	1987-05-24
14	7131	P3087a	1987-05-29
15	7067	P2121	1987-06-15
\.


--
-- Data for TOC entry 55 (OID 17667)
-- Name: salesdetails; Type: TABLE DATA; Schema: public; Owner: webmstr
--

COPY salesdetails (sonum, qty_ordered, qty_shipped, title_id, date_shipped) FROM stdin;
1	75	75	PS2091	1985-09-15
2	10	10	PS2091	1985-09-15
3	20	720	PS2091	1985-09-18
4	25	20	MC3021	1985-09-18
5	15	15	MC3021	1985-09-14
6	10	3	BU1032	1985-09-22
7	3	3	PS2091	1985-09-20
8	5	5	BU1032	1985-09-14
9	25	5	BU1111	1988-03-28
19	35	35	BU2075	1988-03-15
10	15	15	BU7832	1987-10-29
11	10	10	MC2222	1988-01-12
12	30	30	PC1035	1987-05-24
13	50	50	PC8888	1987-05-24
14	20	20	PS1372	1987-05-29
14	25	25	PS2106	1987-04-29
14	15	10	PS3333	1987-05-29
14	25	25	PS7777	1987-06-13
15	40	40	TC3218	1987-06-15
15	20	20	TC4203	1987-05-30
15	20	10	TC7777	1987-06-17
\.


CREATE UNIQUE INDEX pubind ON publishers USING btree (pub_id);


CREATE UNIQUE INDEX auidind ON authors USING btree (au_id);


CREATE INDEX aunmind ON authors USING btree (au_lname, au_fname);


CREATE UNIQUE INDEX titleidind ON titles USING btree (title_id);


CREATE INDEX titleind ON titles USING btree (title);


CREATE UNIQUE INDEX taind ON titleauthors USING btree (au_id, title_id);


CREATE UNIQUE INDEX edind ON editors USING btree (ed_id);


CREATE INDEX ednmind ON editors USING btree (ed_lname, ed_fname);


CREATE UNIQUE INDEX teind ON titleditors USING btree (ed_id, title_id);


CREATE INDEX rstidind ON roysched USING btree (title_id);



