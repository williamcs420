<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>CS420: Assign4</title>
</head>

<style>
    table {background-color:gold;}
</style>

<body>

<%@ page import="java.sql.*" %>

<%@ page errorPage="ErrorPage.jsp" %>

<center>
    <h1>CS420.Spring2011: Assign4</h1>
    <%
        boolean debug = true;

        /* Load the database driver. */
        Class.forName("org.postgresql.Driver");

        /* Establish a connection to database. */
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost/bookbiz", "webmstr", "yacc912");

        /* Create a statement object by which SQL commands may be issued. */
        Statement stmnt = con.createStatement();

        /* Execute the SQL query command. */
        ResultSet result = stmnt.executeQuery("select * from sales natural join salesDetails order by stor_id,sonum,qty_ordered");

        /* Do we have a result set? */
        if (result != null) {
    %>
    <table border="5">
        <th>StoreID</th>
        <th>SalesOrderNum</th>
        <th>PONum</th>
        <th>Date</th>
        <th>Quantity Ordered</th>
        <th>Quantity Shipped</th>
        <th>Title ID</th>
        <th>Date Shipped</th>
        <%
            /* Get each row from the result set */
            while (result.next()) {
                /* Get the data to be placed in the row. */
                int salesOrderNum = result.getInt("sonum");
                String storeID = result.getString("stor_id");
                String ponum = result.getString("ponum");
                Date salesDate = result.getDate("sdate");
                int qtyOrdered = result.getInt("qty_ordered");
                int qtyShipped = result.getInt("qty_shipped");
                String titleId = result.getString("title_id");
                Date dateShipped = result.getDate("date_shipped");
                if (debug) {
                    System.out.println("storeID=" + storeID + " salesOrderNum=" + salesOrderNum);
                }
        %>
        <tr>
            <td align="center"><%= storeID %></td>
            <td align="center"><%= Integer.toString(salesOrderNum) %></td>
            <td align="center"><%= ponum %></td>
            <td align="center"><%= salesDate.toString() %></td>
            <td align="center"><%= Integer.toString(qtyOrdered) %></td>
            <td align="center"><%= Integer.toString(qtyShipped) %></td>
            <td align="center"><%= titleId %></td>
            <td align="center"><%= dateShipped.toString() %></td>
        </tr>
        <%
                }

            } else
                out.print("<p>Bad query; no result set from query command.</p>");

            /* Close the SQL statement object. */
            stmnt.close();

            /* Close the database connection. */
            con.close();
        %>
    </table>
</center>

</body>
</html>
