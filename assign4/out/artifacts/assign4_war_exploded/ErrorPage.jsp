<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<html>
<head>
    <title>CS420: Error</title>
</head>

<body>
<h1>CS420: Error</h1>

<%@ page
        import="java.io.PrintWriter,java.io.StringWriter" %>

<%@ page isErrorPage="true" %>

<%
    /* Safety; do we really have an error. */
    if (exception != null) {
%>

<p>
    The system has encountered a fatal error.
</p>

<p>
    Below is the error message and a stack dump which defines
    the location where the problem was detected.
</p>

<%
            /* Capture the stackTrace to a string buffer; report it. */
            StringWriter buffer = new StringWriter();
            PrintWriter outStr = new PrintWriter(buffer);
            out.println("Error: exception msg= " + exception.getMessage() + "<br>");
            out.println("<pre>");
            exception.printStackTrace(outStr);
            out.println(buffer.toString());
            out.println("</pre>");

            /* Invalidate the session so that no erroneous information is retained. */
            session.invalidate();
    }

%>

</body>
</html>
