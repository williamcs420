import com.sleepycat.je.*;
import com.sleepycat.bind.tuple.IntegerBinding;

import java.io.*;

public class LoadEvents {

    private void run() throws Exception {
        boolean debug = true;

        /* Create a new, transactional database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);
        Environment env = new Environment(new File("CEISDB"), envConfig);

        /* Create a DataBase 'table'. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database theDb = env.openDatabase(null, "events", dbConfig);

        /* Open the data file of new events. */
        BufferedReader input = new BufferedReader(new FileReader("events.data"));

        /* Read and process each line which is an Event. */
        String line;
        DatabaseEntry theKey = new DatabaseEntry();
        DatabaseEntry theData = new DatabaseEntry();
        int count = 0;
        while ((line = input.readLine()) != null) {
            /* Extract the information from the line. */
            String[] factor = line.split("[|]");
            Integer eventid = new Integer(factor[0].trim());
            String title = factor[1].trim();
            String location = factor[2].trim();
            String date = factor[3].trim();
            Boolean onCampus = factor[4].trim().equals("t");
            Integer credit = new Integer(factor[5].trim());
            if (debug)
                System.out.println(String.format("%s %s %s %s %b %s", eventid, title, location, date, onCampus, credit));

            /* Construct a EventRow object. */
            EventRow er = new EventRow(eventid,title,location,date,onCampus,credit);

            /* Create the DatabaseEntry for the key and data. */
            IntegerBinding.intToEntry(eventid, theKey);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(output);
            oos.writeObject(er);
            theData.setData(output.toByteArray());

            /* Save the key/value pair. */
            theDb.put(null, theKey, theData);
            count += 1;
        }
        System.out.printf("count=%d%n", count);

        /* Close the input data file. */
        input.close();

        /* Close the database and it's environment. */
        theDb.close();
        env.close();

    }

    public static void main(String[] args) throws Exception {
        LoadEvents application = new LoadEvents();
        application.run();
    }
}
