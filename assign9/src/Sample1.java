import com.sleepycat.je.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;

/* ------------------------------------------------------------------ */
/* Sample1 illustrates the use of a 'cursor' to move through a table. */
/* ------------------------------------------------------------------ */
public class Sample1 {
    private void run() throws Exception {

        /* Create a database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(false);
        Environment env = new Environment(new File("CEISDB"), envConfig);

        /* Open the 'students' table. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(false);
        dbConfig.setAllowCreate(false);
        Database studentDb = env.openDatabase(null, "students", dbConfig);

        /* Get a cursor. */
        Cursor studentCursor = studentDb.openCursor(null, null);

        /* Setup for table access. */
        DatabaseEntry studentKey = new DatabaseEntry();
        DatabaseEntry studentData = new DatabaseEntry();

        /* Find the first StudentRow entry whose key is greater than zero. */
        byte[] keyBuffer = new byte[4];
        ByteBuffer keybb = ByteBuffer.wrap(keyBuffer);
        keybb.putInt(0); // userid of zero.
        studentKey.setData(keybb.array());
        OperationStatus os = studentCursor.getSearchKeyRange(studentKey, studentData, LockMode.DEFAULT);
        /* OK... process the row and all other rows. */
        while (os == OperationStatus.SUCCESS) {
            /* Convert the retrieved data into a StudentRow object. */
            ByteArrayInputStream input = new ByteArrayInputStream(studentData.getData());
            ObjectInputStream ostream = new ObjectInputStream(input);
            StudentRow ar = (StudentRow) ostream.readObject();

            /* Display the StudentRow data. */
            System.out.printf("student row: %d %s %s.%n", ar.getUserid(), ar.getLastName(), ar.getFirstName());

            /* Find the next StudentRow entry. */
            os = studentCursor.getNext(studentKey, studentData, LockMode.DEFAULT);
        }

        /* Close the cursor(s), the database(s), and it's environment. */
        studentCursor.close();
        studentDb.close();
        env.close();
    }

    public static void main(String[] args) throws Exception {
        Sample1 application = new Sample1();
        application.run();
    }
}
