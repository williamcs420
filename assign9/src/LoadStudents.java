import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.je.*;

import java.io.*;

public class LoadStudents {
    private void run() throws Exception {
        boolean debug = true;

        /* Create a new, transactional database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);
        Environment env = new Environment(new File("CEISDB"), envConfig);

        /* Create a DataBase 'table'. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database theDb = env.openDatabase(null, "students", dbConfig);

        /* Open the data file. */
        BufferedReader input = new BufferedReader(new FileReader("students.data"));

        /* Read and process each line. Each line is a Student.*/
        String line;
        DatabaseEntry theKey = new DatabaseEntry();
        DatabaseEntry theData = new DatabaseEntry();
        int count = 0;
        while ((line = input.readLine()) != null) {
            /* Extract the information from the line. */
            String[] factor = line.split("[|]");
            Integer userid = Integer.parseInt(factor[0].trim());
            String lastName = factor[1].trim();
            String firstName = factor[2].trim();
            String otherNames = factor[3].trim();
            if (debug) System.out.println(String.format("%d %s %s %s", userid, lastName, firstName, otherNames));

            /* Construct a StudentRow object. */
            StudentRow sr = new StudentRow(userid, lastName, firstName, otherNames);

            /* Create the DatabaseEntry for the key and data. */
            IntegerBinding.intToEntry(userid, theKey);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(output);
            oos.writeObject(sr);
            theData.setData(output.toByteArray());

            /* Save the key/value pair. */
            theDb.put(null, theKey, theData);
            count += 1;
        }
        System.out.printf("count=%d%n", count);

        /* Close the input file. */
        input.close();

        /* Close the database and it's environment. */
        theDb.close();
        env.close();

    }

    public static void main(String[] args) throws Exception {
        LoadStudents application = new LoadStudents();
        application.run();
    }
}
