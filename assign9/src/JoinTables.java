import com.sleepycat.je.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

public class JoinTables {
    private void run() throws Exception {

        /* Create a database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(false);
        Environment env = new Environment(new File("CEISDB"), envConfig);

        /* Open the tables. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(false);
        dbConfig.setAllowCreate(false);
        Database eventDb = env.openDatabase(null, "events", dbConfig);
        Database attendDb = env.openDatabase(null, "attendance", dbConfig);
        Database studentDb = env.openDatabase(null, "students", dbConfig);

        /* Get a cursor. */
        Cursor studentCursor = studentDb.openCursor(null, null);

        /* Setup for table access. */
        DatabaseEntry studentKey = new DatabaseEntry();
        DatabaseEntry studentData = new DatabaseEntry();
        DatabaseEntry attendKey = new DatabaseEntry();
        DatabaseEntry attendData = new DatabaseEntry();
        DatabaseEntry eventKey = new DatabaseEntry();
        DatabaseEntry eventData = new DatabaseEntry();

        /* Find the first StudentRow entry whose key is greater than zero. */
        byte[] keyBuffer = new byte[4];
        ByteBuffer keybb = ByteBuffer.wrap(keyBuffer);
        keybb.putInt(0); // userid of zero.
        studentKey.setData(keybb.array());
        OperationStatus os = studentCursor.getSearchKeyRange(studentKey, studentData, LockMode.DEFAULT);

        /* OK... process the row and all other rows. */
        while (os == OperationStatus.SUCCESS) {
            /* Convert the retrieved data into a StudentRow object. */
            ByteArrayInputStream input = new ByteArrayInputStream(studentData.getData());
            ObjectInputStream ostream = new ObjectInputStream(input);
            StudentRow sr = (StudentRow) ostream.readObject();

            attendKey.setData(ByteBuffer.allocate(4).putInt(sr.getUserid().intValue()).array());

            Cursor attendCursor = attendDb.openCursor(null, null);
            OperationStatus osAttend = attendCursor.getSearchKeyRange(attendKey, attendData, LockMode.DEFAULT);
            // natural join on Attendance
            while (osAttend == OperationStatus.SUCCESS) {
                ByteArrayInputStream attendInput = new ByteArrayInputStream(attendData.getData());
                ObjectInputStream attendOStream = new ObjectInputStream(attendInput);
                AttendanceRow ar = (AttendanceRow) attendOStream.readObject();

                if (sr.getUserid().equals(ar.getUserid())) {

                    eventKey.setData(ByteBuffer.allocate(4).putInt(ar.getEventid().intValue()).array());

                    Cursor eventCursor = eventDb.openCursor(null, null);
                    OperationStatus osEvent = eventCursor.getSearchKeyRange(eventKey, eventData, LockMode.DEFAULT);
                    while (osEvent == OperationStatus.SUCCESS) {
                        ByteArrayInputStream eventInput = new ByteArrayInputStream(eventData.getData());
                        ObjectInputStream eventOStream = new ObjectInputStream(eventInput);
                        EventRow er = (EventRow) eventOStream.readObject();

                        if (ar.getEventid().equals(er.getEventid())) {
                            System.out.printf("| %s | %s | %s |\n",sr.getUserid(),ar.getEventid(),er.getTitle());
                        }

                        osEvent = eventCursor.getNext(eventKey, eventData, LockMode.DEFAULT);
                    }

                    eventCursor.close();
                }
                else {
                    break;
                }

                osAttend = attendCursor.getNext(attendKey, attendData, LockMode.DEFAULT);
            }

            attendCursor.close();

            /* Find the next StudentRow entry. */
            os = studentCursor.getNext(studentKey, studentData, LockMode.DEFAULT);
        }

        /* Close the cursor(s), the database(s), and it's environment. */
        studentCursor.close();
        studentDb.close();
        attendDb.close();
        eventDb.close();
        env.close();
    }

    public static void main(String[] args) throws Exception {
        JoinTables application = new JoinTables();
        application.run();
    }
}
