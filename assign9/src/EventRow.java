import java.io.Serializable;

public class EventRow implements Serializable {
    private Integer eventid;
    private String title;
    private String location;
    private String date;
    private Boolean onCampus;
    private Integer credit;

    public EventRow(Integer eventid, String title, String location, String date, Boolean onCampus, Integer credit) {
        this.eventid = eventid;
        this.title = title;
        this.location = location;
        this.date = date;
        this.onCampus = onCampus;
        this.credit = credit;
    }

    public Integer getEventid() {
        return eventid;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public String getDate() {
        return date;
    }

    public Boolean getOnCampus() {
        return onCampus;
    }

    public Integer getCredit() {
        return credit;
    }
}
