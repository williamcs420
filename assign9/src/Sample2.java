import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.je.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;

/* ------------------------------------------------------------------ */
/* Sample2 illustrates finding a table item when the 'key' is known.  */

/* ------------------------------------------------------------------ */
public class Sample2 {
    private void run() throws Exception {

        /* Create a database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(false);
        Environment env = new Environment(new File("CEISDB"), envConfig);

        /* Open the 'students' table. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(false);
        dbConfig.setAllowCreate(false);
        Database studentDb = env.openDatabase(null, "students", dbConfig);

        /* Setup for table access. */
        DatabaseEntry studentKey = new DatabaseEntry();
        DatabaseEntry studentData = new DatabaseEntry();

        /* Now... find the StudentRow. */
        IntegerBinding.intToEntry(610234, studentKey);  // Using a student-id of 610234.
        OperationStatus os = studentDb.get(null, studentKey, studentData, LockMode.DEFAULT);

        /* Did we find the student? */
        if (os == OperationStatus.SUCCESS) {
            /* Convert the retrieved data to a StudentRow object. */
            ByteArrayInputStream input = new ByteArrayInputStream(studentData.getData());
            ObjectInputStream ostream = new ObjectInputStream(input);
            StudentRow sr = (StudentRow) ostream.readObject();

            /* Display the student. */
            System.out.printf("userid=%d lastName= %s firstName=%s%n", sr.getUserid(), sr.getLastName(), sr.getFirstName());

        } else {
            throw new Exception("Error: Failed to find the student.");
        }

        /* Close the database(s), and it's environment. */
        studentDb.close();
        env.close();
    }

    public static void main(String[] args) throws Exception {
        Sample2 application = new Sample2();
        application.run();
    }
}
