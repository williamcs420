import java.io.Serializable;

public class StudentRow implements Serializable {
    private Integer userid;
    private String lastName;
    private String firstName;
    private String otherNames;

    public StudentRow(Integer userid, String lastName, String firstName, String otherNames) {
        this.userid = userid;
        this.lastName = lastName;
        this.firstName = firstName;
        this.otherNames = otherNames;
    }

    public Integer getUserid() {
        return userid;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }
}
