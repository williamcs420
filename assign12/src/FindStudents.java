import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.je.*;

import java.io.*;

public class FindStudents {
    private void run() throws Exception {
        boolean debug = true;

        /* Create a new database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(false);
        envConfig.setAllowCreate(false);
        Environment env = new Environment(new File("ASSIGN12DB"), envConfig);

        /* Create a DataBase 'table'. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(false);
        dbConfig.setAllowCreate(false);
        Database studentDb = env.openDatabase(null, "students", dbConfig);

        /* Open the data file. */
        BufferedReader input = new BufferedReader(new FileReader("students.data"));

        /* Read and process each line. Each line contains a userid.*/
        String line;
        DatabaseEntry studentKey = new DatabaseEntry();
        DatabaseEntry studentData = new DatabaseEntry();
        int errorCount = 0;
        OperationStatus os;
        ByteArrayInputStream dbInput;
        ObjectInputStream ostream;
        while ((line = input.readLine()) != null) {
            /* Extract the information from the line. */
            String[] factor = line.split("[|]");
            Integer userid = Integer.parseInt(factor[0].trim());
            if (debug) System.out.print(String.format("processing %d ", userid));

            /* Now... find the StudentRow. */
            IntegerBinding.intToEntry(userid, studentKey);

            studentDb.get(null,studentKey,studentData,LockMode.READ_COMMITTED);

            dbInput = new ByteArrayInputStream(studentData.getData());
            ostream = new ObjectInputStream(dbInput);

            StudentRow sr = (StudentRow) ostream.readObject();

            System.out.println(sr.getFirstName());

        }
        /* Report the error count. */
        System.out.printf("%nerrorCount=%d%n", errorCount);

        /* Close the input file. */
        input.close();

        /* Close the database and it's environment. */
        studentDb.close();
        env.close();
    }

    public static void main(String[] args) throws Exception {
        FindStudents application = new FindStudents();
        application.run();
    }
}
