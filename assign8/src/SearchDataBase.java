import java.io.BufferedReader;
import java.io.FileReader;

public class SearchDataBase {

    private void run() throws Exception{

        /* Open the input data file. */
        BufferedReader input = new BufferedReader(new FileReader("weather.data"));

        /* Close the input file. */
        input.close();
    }

    public static void main(String[] args) throws Exception {
        SearchDataBase application= new SearchDataBase();
        application.run();
    }
}
