import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.RandomAccessFile;

public class LoadData {

    private void run() throws Exception {
        BufferedReader in = new BufferedReader(new FileReader("weather.data.sample"));
        RandomAccessFile dbfile = new RandomAccessFile("weather.db","rw");
        File file = new File("weather.data.sample");

        String line = in.readLine();

        long numOfTuples = file.length()/line.length();

        System.out.println(numOfTuples);




    }

    private int computeHash(String key, int numberOfBuckets) {
        int sum = 0;
        for(char c : key.toCharArray()) {
            sum += c;
        }
        sum = sum % numberOfBuckets;
        return sum;
    }

    public static void main(String[] args) throws Exception {
        LoadData application = new LoadData();
        application.run();
    }
}
