import java.io.*;

public class ExtractData {
    private void run() throws Exception{
        /* Find all of the data files. */
        File parentDir= new File("2009");
        File[] fileArray= parentDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith("op");
            }
        });

        /* Process each file. */
        BufferedWriter output= new BufferedWriter(new FileWriter("weather.data"));
        for(File f : fileArray){
            /* Open the file as input. */
            BufferedReader input= new BufferedReader(new FileReader("2009/"+f.getName()));

            /* Process the lines in the input file. */
            String line= input.readLine(); // Ignore the first line.
            while((line= input.readLine()) != null){
                /* Split the line so that we can get the first 4 strings. */
                String[] factors= line.split("\\s+");
                /* Define the key. */
                String key= String.format("%s.%s.%s",factors[0],factors[1],factors[2]);
                /* Define the value. */
                String value= factors[3];
                /* Save the key and value to output file. */
                output.write(String.format("%s|%s\n",key,value));
            }

            /* Close the input file. */
            input.close();

            /* Testing!!! */
            // break;
        }
        output.close();
    }

    public static void main(String[] args) throws Exception {
        ExtractData application= new ExtractData();
        application.run();
    }
}
