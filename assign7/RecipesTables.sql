drop table Ingredient_Classes cascade;
CREATE TABLE Ingredient_Classes 
  (IngredientClassID smallint PRIMARY KEY,
   IngredientClassDescription varchar (255));
grant select on Ingredient_Classes to public;

drop table Measurements cascade;
CREATE TABLE Measurements 
  (MeasureAmountID smallint PRIMARY KEY,
   MeasurementDescription varchar (255));
grant select on Measurements to public;

drop table Ingredients cascade;
CREATE TABLE Ingredients 
  (IngredientID int PRIMARY KEY,
   IngredientName varchar (255),
   IngredientClassID smallint REFERENCES Ingredient_Classes (IngredientClassID),
   MeasureAmountID smallint REFERENCES Measurements (MeasureAmountID));
grant select on Ingredients to public;

drop table Recipe_Classes cascade;
CREATE TABLE Recipe_Classes 
  (RecipeClassID smallint PRIMARY KEY,
   RecipeClassDescription varchar (255));
grant select on Recipe_Classes to public;

drop table Recipes cascade;
CREATE TABLE Recipes 
  (RecipeID int PRIMARY KEY,
   RecipeTitle varchar (255),
   RecipeClassID smallint REFERENCES Recipe_Classes (RecipeClassID),
   Preparation varchar (1000),
   Notes varchar (1000));
grant select on Recipes to public;

drop table Recipe_Ingredients cascade;
CREATE TABLE Recipe_Ingredients 
  (RecipeID int NOT NULL REFERENCES Recipes (RecipeID),
   RecipeSeqNo smallint NOT NULL,
   IngredientID int REFERENCES Ingredients (IngredientID),
   MeasureAmountID smallint REFERENCES Measurements (MeasureAmountID),
   Amount real,
   PRIMARY KEY (RecipeID, RecipeSeqNo));
grant select on Recipe_Ingredients to public;
