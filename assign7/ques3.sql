SELECT A.bowlerid, A.bowlerlastname, A.bowlerfirstname, A.CurrentAverage, 
	B.bowlerid, B.bowlerlastname, B.bowlerfirstname, B.CurrentAverage 
	FROM Bowlers A, Bowlers B
	WHERE A.CurrentAverage = B.CurrentAverage
	AND A.bowlerid <> B.bowlerid
	ORDER BY A.bowlerlastname, B.bowlerlastname
