SELECT Recipes.recipeid, Recipes.recipetitle FROM Recipes
	NATURAL JOIN Recipe_Ingredients
	INNER JOIN Ingredients on Recipe_Ingredients.IngredientID = Ingredients.IngredientID
	NATURAL JOIN Ingredient_Classes
	WHERE Ingredient_Classes.IngredientClassDescription = 'Dairy'
	ORDER BY Recipes.recipeid
