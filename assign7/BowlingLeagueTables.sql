drop table tournaments cascade;
CREATE TABLE Tournaments 
  (TourneyID smallint PRIMARY KEY,
   TourneyDate date,
   TourneyLocation varchar (50));
grant select on tournaments to public;

drop table Teams cascade;
CREATE TABLE Teams 
  (TeamID smallint PRIMARY KEY,
   TeamName varchar (50),
   CaptainID smallint NOT NULL);
grant select on Teams to public;

drop table Bowlers cascade;
CREATE TABLE Bowlers 
  (BowlerID smallint PRIMARY KEY,
   BowlerLastName varchar (50),
   BowlerFirstName varchar (50),
   BowlerMiddleInit varchar (1),
   BowlerAddress varchar (50),
   BowlerCity varchar (50),
   BowlerState varchar (2),
   BowlerZip varchar (10),
   BowlerPhoneNumber varchar (15),
   GamesBowled smallint,
   TotalScore int,
   CurrentAverage smallint,
   CurrentHandicap smallint,
   TeamID smallint REFERENCES Teams (TeamID));
grant select on Bowlers to public;

drop table Tourney_Matches cascade;
CREATE TABLE Tourney_Matches 
  (MatchID smallint PRIMARY KEY,
   TourneyID smallint REFERENCES Tournaments (TourneyID),
   Lanes varchar (5),
   OddLaneTeamID smallint REFERENCES Teams (TeamID),
   EvenLaneTeamID smallint REFERENCES Teams (TeamID));
grant select on Tourney_Matches to public;

drop table Match_Games cascade;
CREATE TABLE Match_Games 
  (MatchID smallint NOT NULL REFERENCES Tourney_Matches (MatchID),
   GameNumber smallint NOT NULL,
   WinningTeamID smallint,
   PRIMARY KEY (MatchID, GameNumber));
grant select on Match_Games to public;

drop table Bowler_Scores cascade;
CREATE TABLE Bowler_Scores 
  (MatchID smallint NOT NULL,
   GameNumber smallint NOT NULL,
   BowlerID smallint NOT NULL REFERENCES Bowlers (BowlerID),
   RawScore smallint,
   HandiCapScore smallint,
   WonGame smallint NOT NULL,
   PRIMARY KEY (MatchID, GameNumber, BowlerID),
   FOREIGN KEY (MatchID, GameNumber) REFERENCES Match_Games (MatchID, GameNumber));
grant select on Bowler_Scores to public;
