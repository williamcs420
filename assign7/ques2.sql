SELECT Bowlers.bowlerid, Bowlers.bowlerlastname, Bowlers.bowlerfirstname,
	Tourney_Matches.TourneyID, Bowler_Scores.GameNumber, Bowler_Scores.RawScore FROM Bowlers
	natural join Bowler_Scores
	natural join Tourney_Matches
	WHERE WonGame = '1'
	ORDER BY Bowlers.bowlerlastname, Bowlers.bowlerid, Tourney_Matches.TourneyID,
		 Bowler_Scores.GameNumber
