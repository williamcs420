DROP TABLE Customers;
CREATE TABLE Customers 
  (CustomerID int PRIMARY KEY,
   CustFirstName character varying (25),
   CustLastName character varying (25),
   CustStreetAddress character varying (50),
   CustCity character varying (30),
   CustState character varying (2),
   CustZipCode character varying (10),
   CustAreaCode smallint,
   CustPhoneNumber character varying (8),
   CustPassword character varying not null); 
grant select on Customers to public;

DROP TABLE Employees;
CREATE TABLE Employees 
  (EmployeeID int PRIMARY KEY,
   EmpFirstName character varying (25),
   EmpLastName character varying (25),
   EmpStreetAddress character varying (50),
   EmpCity character varying (30),
   EmpState character varying (2),
   EmpZipCode character varying (10),
   EmpAreaCode smallint,
   EmpPhoneNumber character varying (8)); 
grant select on Employees to public;

DROP TABLE Vendors;
CREATE TABLE Vendors 
  (VendorID smallint PRIMARY KEY,
   VendName character varying (25),
   VendStreetAddress character varying (50),
   VendCity character varying (30),
   VendState character varying (2),
   VendZipCode character varying (10),
   VendPhoneNumber character varying (15),
   VendFaxNumber character varying (15),
   VendWebPage character varying (255),
   VendEMailAddress character varying (50));
grant select on Vendors to public;

DROP TABLE Categories;
CREATE TABLE Categories 
  (CategoryID smallint PRIMARY KEY,
   CategoryDescription character varying (75)); 
grant select on Categories to public;

DROP TABLE Products;
CREATE TABLE Products 
  (ProductNumber smallint PRIMARY KEY,
   ProductName character varying (50),
   ProductDescription character varying (100),
   RetailPrice decimal (10,2),
   QuantityOnHand smallint,
   CategoryID smallint REFERENCES Categories (CategoryID)); 
grant select on Products to public;

DROP TABLE Orders;
CREATE TABLE Orders 
  (OrderNumber smallint PRIMARY KEY,
   OrderDate date,
   ShipDate date,
   CustomerID int REFERENCES Customers (CustomerID),
   EmployeeID int REFERENCES Employees (EmployeeID)); 
grant select on Orders to public;

DROP TABLE Order_Details;
CREATE TABLE Order_Details 
  (OrderNumber smallint NOT NULL REFERENCES Orders (OrderNumber),
   ProductNumber smallint NOT NULL REFERENCES Products (ProductNumber),
   QuotedPrice decimal (10,2),
   QuantityOrdered smallint,
   PRIMARY KEY (OrderNumber, ProductNumber)); 
grant select on Order_Details to public;

DROP TABLE Product_Vendors;
CREATE TABLE Product_Vendors 
  (ProductNumber smallint NOT NULL REFERENCES Products (ProductNumber),
   VendorID smallint NOT NULL REFERENCES Vendors (VendorID),
   WholesalePrice decimal (10,2),
   DaysToDeliver smallint,
   PRIMARY KEY (ProductNumber, VendorID)); 
grant select on Product_Vendors to public;
