import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import org.jdom.filter.Filter;
import org.jdom.input.SAXBuilder;

import java.io.StringReader;
import java.util.Iterator;

public class DataExtractExample {

    private class MyFilter implements Filter {

        public boolean matches(Object o) {
            return o.getClass().getName().equals("org.jdom.Text");
        }
    }

    private void run() {
        try {
            /* Construct a document builder. */
            SAXBuilder builder = new SAXBuilder();

            /* Define the input stream. */
            StringReader input = new StringReader("<result><studentid>1234</studentid><name>Smith, John</name><eventid>2516</eventid><title>History of Computing</title></result>");

            /* Create the document. */
            Document doc = builder.build(input);

            /* Close the input. */
            input.close();

            /* Get the document root. */
            Element root = doc.getRootElement();

            /* Process all of the root's 'text' children. */
            Iterator<Text> iter = root.getDescendants(new MyFilter());
            while (iter.hasNext()) {
                Text t = iter.next();
                /* Print the data associated with an element. */
                System.out.printf("data= %s.%n", t.getText());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        DataExtractExample application = new DataExtractExample();
        application.run();
    }
}
