import com.sleepycat.db.Environment;
import com.sleepycat.db.EnvironmentConfig;
import com.sleepycat.dbxml.*;

import java.io.File;

public class CeisLoad {

    private void run(){
        Environment myEnv;
        File envHome = new File("xmlEnvironment");
        XmlManager myManager = null;
        XmlContainer myContainer = null;
        try {
            /* Setup the environment. */
            EnvironmentConfig envConf = new EnvironmentConfig();
            envConf.setAllowCreate(true);         // If the environment does not
                                                  // exits, create it.
            envConf.setInitializeCache(true);     // Turn on the shared memory
                                                  // region.
            myEnv = new Environment(envHome, envConf);

            /* Create a manager. */
            XmlManagerConfig managerConfig = new XmlManagerConfig();
            managerConfig.setAdoptEnvironment(true);
            managerConfig.setAllowExternalAccess(true);
            myManager = new XmlManager(myEnv, managerConfig);

            /* Create the container. */
            XmlContainerConfig myContainerConfig= new XmlContainerConfig();
            myContainerConfig.setAllowCreate(true);
            myContainerConfig.setAllowValidation(true);
            myContainer= myManager.createContainer("ceis.dbxml",myContainerConfig);

            /* Need an update context for the put. */
            XmlUpdateContext theContext = myManager.createUpdateContext();

            /* Get the input stream. */
            XmlInputStream theStream =  myManager.createLocalFileInputStream("xmlData/ceis.xml");

            /* Do the actual put */
            XmlDocumentConfig myDocumentConfig= new XmlDocumentConfig();
            myContainer.putDocument("ceis.xml",  // The document's name
                                    theStream,   // The actual document.
                                    theContext,  // The update context
                                                 // (required).
                                    myDocumentConfig);  // XmlDocumentConfig object


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Close the container and the manager (manager closes environment). */
            try {
                if (myContainer != null) {
                    myContainer.close();
                    System.out.println("Container closed.");
                }
                if (myManager != null) {
                    myManager.close();
                    System.out.println("Manager closed.");
                }
                System.out.println("End of program.");
            } catch (Exception ce) {
                ce.printStackTrace();
            }
        }

    }

    public static void main(String[] args){
        CeisLoad application= new CeisLoad();
        application.run();
    }
}
