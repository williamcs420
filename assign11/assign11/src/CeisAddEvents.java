import com.sleepycat.db.Environment;
import com.sleepycat.db.EnvironmentConfig;
import com.sleepycat.dbxml.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;

public class CeisAddEvents {

    private void run() {
        boolean debug= true;
        Environment myEnv;
        File envHome = new File("xmlEnvironment");
        XmlManager myManager = null;
        XmlContainer myContainer = null;
        try {
            /* Setup the environment. */
            EnvironmentConfig envConf = new EnvironmentConfig();
            envConf.setAllowCreate(false);
            envConf.setInitializeCache(true);
            myEnv = new Environment(envHome, envConf);

            /* Setup the manager. */
            XmlManagerConfig managerConfig = new XmlManagerConfig();
            managerConfig.setAdoptEnvironment(true);
            managerConfig.setAllowExternalAccess(true);
            myManager = new XmlManager(myEnv, managerConfig);

            /* Setup the container. */
            XmlContainerConfig myContainerConfig = new XmlContainerConfig();
            myContainerConfig.setAllowCreate(false);
            myContainerConfig.setAllowValidation(true);
            myContainer = myManager.openContainer("ceis.dbxml", myContainerConfig);

            /* Setup the query by which we locate the addition point with the data. */
            XmlQueryContext qc = myManager.createQueryContext();
            XmlUpdateContext uc = myManager.createUpdateContext();
            XmlModify mod = myManager.createModify();

            /* Define a query used for Event removal. */
            XmlQueryExpression select = myManager.prepare("doc(\"dbxml:ceis.dbxml/ceis.xml\")/ceis/events/event", qc);

            /* Remove all existing Event objects. */
            /* Note: This is done to simplify multiple executions. */
            mod.addRemoveStep(select);
            select.delete(); // cleanup!

            /* Define a query used for Event addition. */
            select = myManager.prepare("doc(\"dbxml:ceis.dbxml/ceis.xml\")/ceis/events", qc);

            /* Open the data file of new events. */
            BufferedReader input = new BufferedReader(new FileReader("events.data"));

            /* Read and process each line which is an Event. */
            String line;
            while ((line = input.readLine()) != null) {
                /* Extract the information from the line. */
                String[] factor = line.split("[|]");
                String eventid = factor[0].trim();
                String title = factor[1].trim();
                String location = factor[2].trim();
                String date = factor[3].trim();
                Boolean onCampus = factor[4].trim().equals("t");
                String credit = factor[5].trim();
                if(debug)System.out.println(String.format("%s %s %s %s %b %s",eventid,title,location,date,onCampus,credit));

                /* Create an xml Event entry. */
                String newEventContent = String.format("%n<eventid>%s</eventid><title>%s</title><location>%s</location><eventdate>%s</eventdate><oncampus>%b</oncampus><credit>%s</credit>%n", eventid, title, location, date, onCampus, credit);

                /* Save the new Event entry. */
                mod.addAppendStep(select,
                        XmlModify.Element,
                        "event",
                        newEventContent);
            }
            /* Close the input data file. */
            input.close();

            /* OK.... get the document and execute all of the changes defined above. */
            XmlDocument retDoc = myContainer.getDocument("ceis.xml");
            XmlValue docValue = new XmlValue(retDoc);
            mod.execute(docValue, qc, uc);

            /* Save the modified document to a file for viewing/checking. */
            String mssg = docValue.asString();
            FileOutputStream output = new FileOutputStream("ceis.modified.xml");
            output.write(mssg.getBytes());
            output.close();

            /* Cleanup. */
            docValue.delete();
            retDoc.delete();
            select.delete();
            mod.delete();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Close the container and the manager (manager closes environment). */
            try {
                if (myContainer != null) {
                    myContainer.close();
                    System.out.println("Container closed.");
                }
                if (myManager != null) {
                    myManager.close();
                    System.out.println("Manager closed.");
                }
                System.out.println("End of program.");
            } catch (Exception ce) {
                ce.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        CeisAddEvents application = new CeisAddEvents();
        application.run();
    }
}
