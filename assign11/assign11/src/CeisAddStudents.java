import com.sleepycat.db.Environment;
import com.sleepycat.db.EnvironmentConfig;
import com.sleepycat.dbxml.*;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileOutputStream;

public class CeisAddStudents {

    private void run() {
        boolean debug= true;
        Environment myEnv;
        File envHome = new File("xmlEnvironment");
        XmlManager myManager = null;
        XmlContainer myContainer = null;
        try {
            /* Setup the environment. */
            EnvironmentConfig envConf = new EnvironmentConfig();
            envConf.setAllowCreate(false);
            envConf.setInitializeCache(true);
            myEnv = new Environment(envHome, envConf);

            /* Setup the manager. */
            XmlManagerConfig managerConfig = new XmlManagerConfig();
            managerConfig.setAdoptEnvironment(true);
            managerConfig.setAllowExternalAccess(true);
            myManager = new XmlManager(myEnv, managerConfig);

            /* Setup the container. */
            XmlContainerConfig myContainerConfig = new XmlContainerConfig();
            myContainerConfig.setAllowCreate(false);
            myContainerConfig.setAllowValidation(true);
            myContainer = myManager.openContainer("ceis.dbxml", myContainerConfig);

            /* Setup the query by which we locate the addition point with the data. */
            XmlQueryContext qc = myManager.createQueryContext();
            XmlUpdateContext uc = myManager.createUpdateContext();
            XmlModify mod = myManager.createModify();

            /* Define a query used for Student removal. */
            XmlQueryExpression select = myManager.prepare("doc(\"dbxml:ceis.dbxml/ceis.xml\")/ceis/students/student", qc);

            /* Remove all existing Student objects. */
            /* Note: This is done to simplify multiple executions. */
            mod.addRemoveStep(select);
            select.delete(); // cleanup!

            /* Define a query used for Student addition. */
            select = myManager.prepare("doc(\"dbxml:ceis.dbxml/ceis.xml\")/ceis/students", qc);

            /* Open the data file of new stdents. */
            BufferedReader input = new BufferedReader(new FileReader("students.data"));

            /* Read and process each line which is a Student. */
            String line;
            while ((line = input.readLine()) != null) {
                /* Extract the information from the line. */
                String[] factor = line.split("[|]");
                String userid = factor[0].trim();
                String lastName = factor[1].trim();
                String firstName = factor[2].trim();
                String otherNames = factor[3].trim();
                if (debug) System.out.println(String.format("%s %s %s %s", userid, lastName, firstName, otherNames));

                /* Create an xml Student entry. */
                String newStudentContent = String.format("%n<studentid>%s</studentid><lastname>%s</lastname><firstname>%s</firstname><othernames>%s</othernames><attendance></attendance>%n", userid, lastName, firstName, otherNames);

                /* Save the new Student entry. */
                mod.addAppendStep(select,
                        XmlModify.Element,
                        "student",
                        newStudentContent);
            }
            /* Close the input data file. */
            input.close();

            /* OK.... get the document and execute all of the changes defined above. */
            XmlDocument retDoc = myContainer.getDocument("ceis.xml");
            XmlValue docValue = new XmlValue(retDoc);
            mod.execute(docValue, qc, uc);

            /* Save the modified document to a file for viewing/checking. */
            String mssg = docValue.asString();
            FileOutputStream output = new FileOutputStream("ceis.modified.xml");
            output.write(mssg.getBytes());
            output.close();

            /* Cleanup. */
            docValue.delete();
            retDoc.delete();
            select.delete();
            mod.delete();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /* Close the container and the manager (manager closes environment). */
            try {
                if (myContainer != null) {
                    myContainer.close();
                    System.out.println("Container closed.");
                }
                if (myManager != null) {
                    myManager.close();
                    System.out.println("Manager closed.");
                }
                System.out.println("End of program.");
            } catch (Exception ce) {
                ce.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        CeisAddStudents application = new CeisAddStudents();
        application.run();
    }
}
