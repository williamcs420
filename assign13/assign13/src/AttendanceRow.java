import java.io.Serializable;

public class AttendanceRow implements Serializable {
    private Integer userid;
    private Integer eventid;
    private Integer reasonCode;

    public AttendanceRow(Integer userid, Integer eventid, Integer reasonCode) {
        this.userid = userid;
        this.eventid = eventid;
        this.reasonCode = reasonCode;
    }

    public Integer getUserid() {
        return userid;
    }

    public Integer getEventid() {
        return eventid;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }
}
