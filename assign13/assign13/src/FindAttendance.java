import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.je.*;

import java.io.*;
import java.nio.ByteBuffer;

public class FindAttendance {
    private void run() throws Exception {
        boolean debug = true;

        /* Create a new database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(false);
        envConfig.setAllowCreate(false);
        Environment env = new Environment(new File("ASSIGN13DB"), envConfig);

        /* Create a DataBase 'table'. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(false);
        dbConfig.setAllowCreate(false);
        Database attendDb = env.openDatabase(null, "attendance", dbConfig);

        /* Find attendance information for userid == 654364.*/
        DatabaseEntry attendKey = new DatabaseEntry();
        DatabaseEntry attendData = new DatabaseEntry();

        /* Get a cursor. */
        Cursor attendCursor = attendDb.openCursor(null, null);

        /* Find the first AttendanceRow entry whose key is greater than 654364,zero. */
        byte[] keyBuffer = new byte[16];
        ByteBuffer keybb = ByteBuffer.wrap(keyBuffer);
        keybb.putInt(654364); // userid
        keybb.putInt(0); // eventid
        attendKey.setData(keybb.array());
        int attendCount= 0;
        OperationStatus os = attendCursor.getSearchKeyRange(attendKey, attendData, LockMode.DEFAULT);
        while (os == OperationStatus.SUCCESS) {
            /* Convert the retrieved data to an AttendanceRow object. */
            ByteArrayInputStream input = new ByteArrayInputStream(attendData.getData());
            ObjectInputStream ostream = new ObjectInputStream(input);
            AttendanceRow ar = (AttendanceRow) ostream.readObject();

	    /* ???? Add code here. ???? */
        }
        /* Report the attendance count for this student. */
        System.out.printf("%nattendCount=%d%n",attendCount);

        /* Close the cursor, the database and it's environment. */
        attendCursor.close();
        attendDb.close();
        env.close();

    }

    public static void main(String[] args) throws Exception {
        FindAttendance application = new FindAttendance();
        application.run();
    }
}
