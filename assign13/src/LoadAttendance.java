import com.sleepycat.je.*;

import java.io.*;
import java.nio.ByteBuffer;

public class LoadAttendance {

    private void run() throws Exception {
        boolean debug = true;

        /* Create a new, transactional database environment */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);
        Environment env = new Environment(new File("ASSIGN13DB"), envConfig);

        /* Create a DataBase 'table'. */
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database theDb = env.openDatabase(null, "attendance", dbConfig);

        /* Open the data file of new events. */
        BufferedReader input = new BufferedReader(new FileReader("attendance.data"));

        /* Read and process each line which contain Attendance. */
        String line;
        DatabaseEntry theKey = new DatabaseEntry();
        DatabaseEntry theData = new DatabaseEntry();
        int count = 0;
        while ((line = input.readLine()) != null) {
            /* Extract the information from the line. */
            String[] factor = line.split("[|]");
            Integer userid = new Integer(factor[0].trim());
            Integer eventid = new Integer(factor[1].trim());
            Integer reasonCode = new Integer(factor[2].trim());
            if (debug) System.out.println(String.format("%d %d %d", userid, eventid, reasonCode));

            /* Construct an AttendanceRow object. */
            AttendanceRow ar = new AttendanceRow(userid, eventid, reasonCode);

            /* Create the DatabaseEntry for the key and data. */
            /* Note: the key is a compound-key composed of userid and eventid. */
            byte[] keyBuffer = new byte[16];
            ByteBuffer keybb = ByteBuffer.wrap(keyBuffer);
            keybb.putInt(userid);
            keybb.putInt(eventid);
            theKey.setData(keybb.array());
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(output);
            oos.writeObject(ar);
            theData.setData(output.toByteArray());

            /* Save the key/value pair. */
            theDb.put(null, theKey, theData);
            count += 1;
        }
        System.out.printf("count=%d%n", count);

        /* Close the input data file. */
        input.close();

        /* Close the database and it's environment. */
        theDb.close();
        env.close();

    }

    public static void main(String[] args) throws Exception {
        LoadAttendance application = new LoadAttendance();
        application.run();
    }
}
